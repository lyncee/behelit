import React from 'react'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'

import Layout from '../components/layout'
import { MarkdownInput, MultiSelectInput, PasswordInput, SelectInput, TextInput, TextAreaInput } from '../../src'

const items = [
  { text: 'Item 1', value: 1 },
  { text: 'Item 2', value: 2 },
  { text: 'Item 3', value: 3 },
  { text: 'Item 4', value: 4 },
  { text: 'Item 5', value: 5 }
]

storiesOf('Form', module)
  .addDecorator(story => (<Layout>{story()}</Layout>))
  .addDecorator((story, context) => withInfo({ text: 'Documentation', inline: true })(story)(context))
  .add('MarkdownInput', () => <MarkdownInput />)
  .add('MarkdownInput with borderColor', () => <MarkdownInput borderColor='green' />)
  .add('MarkdownInput with preview', () => <MarkdownInput previewModeEnabled />)
  .add('MultiSelectInput', () => <MultiSelectInput items={items} />)
  .add('MultiSelectInput with borderColor', () => <MultiSelectInput items={items} borderColor='green' />)
  .add('PasswordInput', () => <PasswordInput />)
  .add('PasswordInput with borderColor', () => <PasswordInput borderColor='green' />)
  .add('SelectInput', () => <SelectInput items={items} />)
  .add('SelectInput with borderColor', () => <SelectInput items={items} borderColor='green' />)
  .add('SelectInput with search', () => <SelectInput items={items} searchEnabled />)
  .add('TextInput', () => <TextInput />)
  .add('TextInput with borderColor', () => <TextInput borderColor='green' />)
  .add('TextAreaInput', () => <TextAreaInput />)
  .add('TextAreaInput with borderColor', () => <TextAreaInput borderColor='green' />)
