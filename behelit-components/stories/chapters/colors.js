import React from 'react'
import styled from 'styled-components'
import { storiesOf } from '@storybook/react'

import { keysIn } from 'ramda'
import Layout from '../components/layout'
import { Palette } from '../../src/Colors'

const ColorLayout = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;
  flex-wrap: wrap;
`
const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 150px;
  height: 200px;
  margin: 5px;
`
const Sample = styled.div`
  width: 100%;
  height: 150px;
  background-color: ${props => props.bgColor};
  border: 1px solid #CDCDCD;
`
const Code = styled.div`
  width: 100%;
  height: 30px;
  font-size: 16px;
  font-weight: 500;
  text-align: center;
  border-left: 1px solid #CDCDCD;
  border-right: 1px solid #CDCDCD;
  border-bottom: 1px solid #CDCDCD;
`

const PaletteLayout = (props) => {
  const keys = keysIn(Palette)

  return (
    <ColorLayout>
      { keys.map(function (key, index) {
        return (
          <Container key={index}>
            <Sample bgColor={Palette[key]} />
            <Code>{key}</Code>
          </Container>
        )
      })}
    </ColorLayout>
  )
}

storiesOf('Colors', module)
  .addDecorator(story => (<Layout>{story()}</Layout>))
  .add('Palette', () => <PaletteLayout />)
