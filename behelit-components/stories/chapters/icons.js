import React from 'react'
import styled from 'styled-components'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'

import { keysIn } from 'ramda'
import Layout from '../components/layout'
import { Devicon, Icon } from '../../src'
import DeviconMap from '../../src/Icons/Devicon/DeviconMap'

const IconWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;
  flex-wrap: wrap;
`
const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 150px;
  height: 175px;
  margin: 5px;
`
const Sample = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
`
const Code = styled.div`
  display: block;
  width: 100%;
  height: 25px;
  border: 1px solid #CDCDCD;
  box-sizing: border-box;
  font-size: 16px;
  font-weight: 500;
  text-align: center;
`
const IconComponent = props => (
  <Container>
    <Sample>
      <Devicon name={props.name} />
    </Sample>
    <Code>{props.name}</Code>
  </Container>
)

const IconLayout = props => (
  <IconWrapper>
    {props.children}
  </IconWrapper>
)

storiesOf('Icons', module)
  .addDecorator(story => (<Layout>{story()}</Layout>))
  .addDecorator((story, context) => withInfo({ text: 'Documentation', inline: true })(story)(context))
  .add('Devicon', () =>
    <IconLayout>
      { keysIn(DeviconMap).map((value, index) => {
        return <IconComponent key={index} name={value} size='40px' />
      })}
    </IconLayout>)
  .add('Icon', () => <Icon name='android' />)
  .add('Icon with size', () => <Icon name='android' size='54px' />)
  .add('Icon with weight', () => <Icon name='android' size='54px' weight={700} />)
  .add('Icon with color', () => <Icon name='android' size='54px' color='brand-primary' />)
  .add('Icon with cursor', () => <Icon name='android' size='54px' color='brand-primary' cursor />)
