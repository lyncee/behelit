import React from 'react'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'

import Layout from '../components/layout'
import { Link } from '../../src'

storiesOf('Links', module)
  .addDecorator(story => (<Layout>{story()}</Layout>))
  .addDecorator((story, context) => withInfo({ text: 'Documentation', inline: true })(story)(context))
  .add('Link', () => <Link>This is a link</Link>)
  .add('Link with size', () => <Link size='12px'>This is a link</Link>)
  .add('Link with weight', () => <Link weight={900}>This is a bold link</Link>)
  .add('Link with color', () => <Link color='brand-primary'>This is a red link</Link>)
  .add('Link uppercase', () => <Link uppercase>This is an uppercase link</Link>)
