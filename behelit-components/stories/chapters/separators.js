import React from 'react'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'

import Layout from '../components/layout'
import { Separator } from '../../src'

const sample = 'Lorem ipsum dolor sit amet'

storiesOf('Separator', module)
  .addDecorator(story => (<Layout>{story()}</Layout>))
  .addDecorator((story, context) => withInfo({ text: 'Documentation', inline: true })(story)(context))
  .add('Separator', () => <Separator />)
  .add('Separator with left content', () => <Separator align='left'>{sample}</Separator>)
  .add('Separator with right content', () => <Separator align='right'>{sample}</Separator>)
  .add('Separator with center content', () => <Separator align='center'>{sample}</Separator>)
