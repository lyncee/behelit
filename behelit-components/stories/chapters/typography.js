
import React from 'react'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'

import Layout from '../components/layout'
import { Text, TextGroup } from '../../src'

const sample = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.Cras id tortor lectus.Pellentesque pulvinar sit amet massa quis auctor.Ut consectetur dui mi, eu vestibulum felis ornare sit amet.Sed vel interdum massa, a gravida arcu.Vestibulum sed dictum elit.Nulla pharetra euismod quam, ut iaculis nisi tincidunt et.Donec justo neque, pulvinar sit amet sollicitudin nec, fermentum vel sem.Mauris varius ultrices viverra.Donec condimentum velit id aliquam aliquet.Donec dapibus, tortor vehicula mollis venenatis, felis nisi porttitor nisl, vitae imperdiet leo tortor sed eros.'

storiesOf('Typography', module)
  .addDecorator(story => (<Layout>{story()}</Layout>))
  .addDecorator((story, context) => withInfo({ text: 'Documentation', inline: true })(story)(context))
  .add('Text', () => <Text>{sample}</Text>)
  .add('Text with size', () => <Text size='12px'>{sample}</Text>)
  .add('Text with weight', () => <Text weight={900}>{sample}</Text>)
  .add('Text with color', () => <Text color='brand-primary'>{sample}</Text>)
  .add('Text capitalize', () => <Text capitalize>{sample}</Text>)
  .add('Text italic', () => <Text italic>{sample}</Text>)
  .add('Text uppercase', () => <Text uppercase>{sample}</Text>)
  .add('Text underline', () => <Text underline>{sample}</Text>)
  .add('TextGroup not inline', () =>
  (
    <TextGroup>
      <Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Text>
      <Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Text>
    </TextGroup>
  ))
  .add('TextGroup inline', () =>
  (
    <TextGroup inline>
      <Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Text>
      <Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Text>
    </TextGroup>
  ))
  .add('TextGroup nowrap', () =>
  (
    <TextGroup nowrap>
      <Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</Text>
      <Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Text>
    </TextGroup>
  ))
