import React from 'react'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'

import Layout from '../components/layout'
import { Button, ButtonGroup } from '../../src'

storiesOf('Buttons', module)
  .addDecorator(story => (<Layout>{story()}</Layout>))
  .addDecorator((story, context) => withInfo({ text: 'Documentation', inline: true })(story)(context))
  .add('Button primary', () => <Button nature='primary'>Button</Button>)
  .add('Button secondary', () => <Button nature='secondary'>Button</Button>)
  .add('Button fullwidth', () => <Button fullwidth>Button fullwidth</Button>)
  .add('Button disabled', () => <Button disabled>Button disabled</Button>)
  .add('Button uppercase', () => <Button uppercase>Button uppercase</Button>)
  .add('ButtonGroup with 2 buttons', () => (<ButtonGroup><Button>Button 1</Button><Button>Button 2</Button></ButtonGroup>))
  .add('ButtonGroup with 3 buttons', () => (<ButtonGroup><Button>Button 1</Button><Button>Button 2</Button><Button>Button 3</Button></ButtonGroup>))
