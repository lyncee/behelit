import React from 'react'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'

import Layout from '../components/layout'
import { Loader, Spinner } from '../../src'

storiesOf('Loaders', module)
  .addDecorator(story => (<Layout>{story()}</Layout>))
  .addDecorator((story, context) => withInfo({ text: 'Documentation', inline: true })(story)(context))
  .add('Loader', () => <Loader />)
  .add('Spinner', () => <Spinner />)
  .add('Loader with color', () => <Loader color='green' />)
  .add('Loader with height and width', () => <Loader width='100px' height='50px' />)
