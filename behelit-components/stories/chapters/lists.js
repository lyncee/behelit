import React from 'react'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'

import Layout from '../components/layout'
import { List, ListItem } from '../../src'

storiesOf('Lists', module)
  .addDecorator(story => (<Layout>{story()}</Layout>))
  .addDecorator((story, context) => withInfo({ text: 'Documentation', inline: true })(story)(context))
  .add('List', () =>
    <List>
      <ListItem>This is my first ListItem</ListItem>
      <ListItem>This is my second ListItem</ListItem>
      <ListItem>This is my third ListItem</ListItem>
    </List>
  )
