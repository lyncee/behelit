import React from 'react'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'

import Layout from '../components/layout'
import { Toast } from '../../src'

const sample = 'Lorem ipsum dolor sit amet'

storiesOf('Toasts', module)
  .addDecorator(story => (<Layout>{story()}</Layout>))
  .addDecorator((story, context) => withInfo({ text: 'Documentation', inline: true })(story)(context))
  .add('Toast info', () => <Toast nature='info'>{sample}</Toast>)
  .add('Toast success', () => <Toast nature='success'>{sample}</Toast>)
  .add('Toast error', () => <Toast nature='error'>{sample}</Toast>)
