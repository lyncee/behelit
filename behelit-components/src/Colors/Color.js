import Palette from './Palette'

const Color = color => Palette[color]

export default Color
