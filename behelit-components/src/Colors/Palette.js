export default {
  // Brand
  'brand-primary': '#990000',
  'brand-secondary': '#545456',
  'brand-tertiary': '#B2D5E5',
  'brand-quaternary': '#E3EFF5',
  // Grays
  'gray-1': '#F3F3F3',
  'gray-2': '#CCCCCC',
  'gray-3': '#999B9E',
  'gray-4': '#757679',
  'gray-5': '#545456',
  'gray-6': '#383838',
  // Others
  'white': '#FFFFFF',
  'red': '#990000',
  'dark-cyan': '#008B8B',
  'green': '#009900',
  'black': '#000000'
}
