import { injectGlobal } from 'styled-components'

import CookieRegular from './Cookie-Regular.ttf'

const FontFace = (name, ttf) => {
  return (`
    @font-face {
      font-family: '${name}';
      src: url('${ttf}') format('truetype');
      font-weight: 400;
      font-style: normal;
      font-stretch: normal;
    }
  `)
}

// Fonts management
injectGlobal`
  ${FontFace('Cookie', CookieRegular)}
`
