import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { transparentize, lighten } from 'polished'

import { Color } from '../Colors'
import { Icon } from '../Icons'

const Wrapper = styled.div`
  height: 65px;
  width: 100%;
  background-color: ${props => Color('white')};


  @media(min-width: 768px) { width: 500px; }
`
const Container = styled.div`
  position: relative;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  height: 100%;
  padding: 15px 10px 15px 25px;
  box-sizing: border-box;
  background: ${props => transparentize(0.9, Color(props.color))};
  color: ${props => Color(props.color)};
  font-family: 'Roboto', sans-serif;
  font-size: 16px;
  font-weight: 400;
  border-top: 6px solid ${props => transparentize(0.8, Color(props.color))};
  box-shadow: 0 2px 4px rgba(0, 0, 0, .2);
  -moz-osx-font-smoothing: grayscale;
  -webkit-font-smoothing: antialiased;

  & :last-child {
    cursor: pointer;
    font-size: 20px;
    font-weight: 400;
    &:hover { color: ${props => lighten(0.1, Color(props.color))}!important; }
  }
`
const Content = styled.span`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
  flex: 100%;
  margin-right: 10px;
  box-sizing: border-box;
`

const selectColor = type => {
  switch (type) {
    case 'success': return 'green'
    case 'error': return 'red'
    case 'info': return 'dark-cyan'
    default: return 'brand-primary'
  }
}

const Toast = props => {
  const { nature, onClose, children } = props
  const color = selectColor(nature)

  return (
    <Wrapper>
      <Container color={color}>
        <Content>{children}</Content>
        <Icon name='close' color={color} onClick={onClose} />
      </Container>
    </Wrapper>
  )
}

Toast.propTypes = {
  nature: PropTypes.oneOf(['success', 'error', 'info']),
  onClose: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired
}

Toast.defaultProps = {
  nature: 'info'
}

export default Toast
