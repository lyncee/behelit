import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { keysIn, prop } from 'ramda'

import DeviconMap from './DeviconMap'

const Wrapper = styled.div`
  width: ${props => props.width};
  height: ${props => props.height};
  cursor: ${props => props.cursor === 1 ? 'pointer' : 'default'};
`
const Devicon = props => {
  const { name, width, height, cursor } = props
  return (
    <Wrapper width={width} height={height} cursor={cursor ? 1 : 0}>
      <img src={prop(name, DeviconMap)} />
    </Wrapper>
  )
}

Devicon.propTypes = {
  name: PropTypes.oneOf(keysIn(DeviconMap)).isRequired,
  width: PropTypes.string,
  height: PropTypes.string,
  cursor: PropTypes.bool
}

Devicon.defaultProps = {
  width: '100px',
  height: 'auto',
  cursor: false
}

export default Devicon
