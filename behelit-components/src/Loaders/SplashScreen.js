import React from 'react'
import styled from 'styled-components'

import Loader from './Loader'

const Wrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
  background-color: transparent;
`

const SplashScreen = props => {
  return (
    <Wrapper>
      <Loader height='100px' width='150px' />
    </Wrapper>
  )
}

export default SplashScreen
