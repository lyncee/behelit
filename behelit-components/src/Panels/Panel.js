import React from 'react'
import styled from 'styled-components'

import { Color } from '../Colors'

const Wrapper = styled.div`
  position: relative;
  padding: 30px;
  box-sizing: border-box;
  background-color: ${Color('gray-1')};
  border: 1px solid ${Color('gray-2')};
  border-radius: 5px;
`

const Panel = props => {
  return (
    <Wrapper>
      {props.children}
    </Wrapper>
  )
}

export default Panel
