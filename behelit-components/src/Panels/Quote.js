import React from 'react'
import styled from 'styled-components'

import { Color } from '../Colors'
import { Icon } from '../Icons'

const Wrapper = styled.div`
  position: relative;
  padding: 30px;
  box-sizing: border-box;
  background-color: ${Color('gray-1')};
  border: 1px solid ${Color('gray-2')};
  border-radius: 5px;
`
const LeftQuote = styled.div`
  position: absolute;
  top: 10px;
  left: 10px;
`
const RightQuote = styled.div`
  position: absolute;
  bottom: 10px;
  right: 10px;
`

const Quote = (props) => {
  return (
    <Wrapper>
      <LeftQuote>
        <Icon name='quote-left' size='14px' color='gray-4' />
      </LeftQuote>
      {props.children}
      <RightQuote>
        <Icon name='quote-right' size='14px' color='gray-4' />
      </RightQuote>
    </Wrapper>
  )
}

export default Quote
