import React from 'react'
import styled from 'styled-components'

import { Color } from '../Colors'

const BaseNavItem = styled.li`
  padding: 10px 15px;
  box-sizing: border-box;

  & > a {
    font-family: 'Roboto', sans-serif;
    font-size: 16px;
    font-weight: 500;
    color: ${Color('white')};
    text-transform: uppercase;
    text-decoration: none;
    cursor: pointer;
    transition: 0.3s;

    &:hover { color: ${Color('brand-primary')}; }
    &.active { color: ${Color('brand-primary')}; }
  }
`

const NavbarNavItem = props => {
  const { children, ...rest } = props

  return (
    <BaseNavItem {...rest}>
      {children}
    </BaseNavItem>
  )
}

export default NavbarNavItem
