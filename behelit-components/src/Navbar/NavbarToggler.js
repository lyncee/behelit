import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { Color } from '../Colors'
import { Icon } from '../Icons'

const Wrapper = styled.div`
  display: block;
  cursor: pointer;
  padding: 10px;
  margin-right: 10px;
  background-color: transparent;
  border: none;
  font-size: 24px;
  color: ${Color('white')};

  @media(min-width: 768px) { display: none; }

  & > span { outline: none; }
`

const NavbarToggler = props => {
  const { onToggle, toggled } = props
  const name = toggled ? 'close' : 'bars'

  return (
    <Wrapper onClick={onToggle}>
      <Icon name={name} size='24px' color='white' cursor hover />
    </Wrapper>
  )
}

NavbarToggler.propTypes = {
  onToggle: PropTypes.func.isRequired
}

export default NavbarToggler
