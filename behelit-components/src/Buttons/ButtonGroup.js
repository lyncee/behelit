import React from 'react'
import styled from 'styled-components'

const BaseButtonGroup = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;  
  width: 100%;
`
const ButtonGroup = props => {
  const { children, ...rest } = props

  return (
    <BaseButtonGroup {...rest}>
      {children}
    </BaseButtonGroup>
  )
}

export default ButtonGroup
