import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { darken } from 'polished'

import { Color } from '../Colors'

const BaseButton = styled.button.attrs({
  type: 'submit'
})`
  width: ${props => props.fullwidth ? '100%' : 'auto'};
  min-width: 140px;
  height: 40px;
  padding: 10px 15px;
  box-sizing: border-box;
  user-select: none;
  text-align: center;
  text-decoration: none;
  vertical-align: middle;
  letter-spacing: normal;
  transition: all .2s ease-in-out;
  white-space: nowrap;
  line-height: 1;
  text-transform: ${props => props.uppercase ? 'uppercase' : 'none'};
  font-family: 'Roboto', sans-serif;
  font-size: 16px;
  font-weight: 300;
  cursor: ${props => props.disabled ? 'not-allowed' : 'pointer'};
  color: ${props => Color(props.color)};
  background-color: ${props => Color(props.backgroundColor)};
  border: none;

  &:hover {
    background-color: ${props => props.disabled ? 'none' : darken(0.1, (Color(props.backgroundColor)))};
  }
  &:focus { outline:0; }
`

const selectColor = (nature, disabled) => {
  if (disabled) { return { color: 'white', backgroundColor: 'gray-3' } }

  switch (nature) {
    case 'primary': return { color: 'white', backgroundColor: 'brand-primary' }
    case 'secondary': return { color: 'white', backgroundColor: 'brand-secondary' }
    default: return { color: 'white', backgroundColor: 'brand-primary' }
  }
}

const Button = (props) => {
  const { nature, disabled, children, ...rest } = props
  const { color, backgroundColor } = selectColor(nature, disabled)

  return (
    <BaseButton color={color} backgroundColor={backgroundColor} disabled={disabled} {...rest}>
      {children}
    </BaseButton>
  )
}

Button.propTypes = {
  nature: PropTypes.oneOf(['primary', 'secondary']),
  fullwidth: PropTypes.bool,
  disabled: PropTypes.bool,
  uppercase: PropTypes.bool
}

Button.defaultProps = {
  nature: 'primary',
  fullwidth: false,
  disabled: false,
  uppercase: false
}

export default Button
