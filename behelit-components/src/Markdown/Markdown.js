import React from 'react'
import PropTypes from 'prop-types'
import ReactMarkdown from 'react-markdown'

import PrismCode from './PrismCode'
import { Link } from '../Links'
import { List, ListItem } from '../Lists'
import { Separator } from '../Separators'
import { Text } from '../Typography'

const codeRenderer = props => <PrismCode language={props.language} code={props.value} />

const inlineCodeRenderer = props => <PrismCode language={props.language} code={props.value} inline />

const headingRenderer = props => {
  switch (props.level) {
    case 1: return <Text tag='h1' size='32px' weight={300} color='brand-primary' uppercase>{props.children}</Text>
    case 2: return <Text tag='h2' size='24px' weight={300} color='brand-primary' uppercase underline>{props.children}</Text>
    case 3: return <Text tag='h3' size='20.8px' weight={300} color='gray-6'>{props.children}</Text>
    case 4: return <Text tag='h4' size='16px' weight={300} color='gray-6'>{props.children}</Text>
    case 5: return <Text tag='h5' size='12.8px' weight={300} color='gray-6'>{props.children}</Text>
    case 6: return <Text tag='h6' size='11.2px' weight={300} color='gray-6'>{props.children}</Text>
    default: return <span>{props.children.join(' ')}</span>
  }
}

const linkRenderer = props => <Link size='16px' weight={300} color='dark-cyan' href={props.href} target='_blank'>{props.children}</Link>

const linkReferenceRenderer = props => <Link size='16px' weight={300} color='dark-cyan' href={props.href} target='_blank'>{props.children}</Link>

const listReference = props => <List>{props.children}</List>

const listItemReference = props => <ListItem>{props.children}</ListItem>

const paragraphRenderer = props => <Text tag='p' size='16px' weight={300} color='gray-6'>{props.children}</Text>

const rootRenderer = props => props.children

const thematicBreakRenderer = props => <Separator />

const renderers = {
  'code': codeRenderer,
  'heading': headingRenderer,
  'inlineCode': inlineCodeRenderer,
  'link': linkRenderer,
  'linkReference': linkReferenceRenderer,
  'list': listReference,
  'listItem': listItemReference,
  'paragraph': paragraphRenderer,
  'root': rootRenderer,
  'thematicBreak': thematicBreakRenderer
}

const Markdown = props => <ReactMarkdown source={props.content} renderers={renderers} />

Markdown.propTypes = {
  content: PropTypes.string.isRequired
}

export default Markdown
