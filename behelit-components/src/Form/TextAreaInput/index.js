import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { keysIn } from 'ramda'

import { Color, Palette } from '../../Colors'

const Input = styled.textarea`
  display: block;
  width: 100%;
  height: auto;
  padding: 6px 12px;
  box-sizing: border-box;
  font-size: 14px;
  line-height: 1.42;
  color: ${Color('gray-5')};
  background-color: ${Color('white')};
  background-image: none;
  outline-width: 0;
  user-select: text;
  resize: none;
  border: 1px solid  ${props => Color(props.borderColor)};
  &::-webkit-input-placeholder { color: ${Color('gray-2')}; }
`

const TextAreaInput = props => <Input {...props} />

TextAreaInput.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func,
  borderColor: PropTypes.oneOf(keysIn(Palette)),
  rows: PropTypes.number
}

TextAreaInput.defaultProps = {
  borderColor: 'gray-2',
  rows: 5
}

export default TextAreaInput
