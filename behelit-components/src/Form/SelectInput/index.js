import React from 'react'
import PropTypes from 'prop-types'
import { contains, equals, filter, head, isNil, keysIn, prop, toUpper } from 'ramda'

import { Palette } from '../../Colors'
import SelectInput from './template.js'

class SelectInputContainer extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      value: this.props.value,
      search: '',
      expanded: false
    }
    this.handleBlur = this.handleBlur.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleClick = this.handleClick.bind(this)
    this.handleFocus = this.handleFocus.bind(this)
  }

  componentWillReceiveProps (nextProps) {
    if (!equals(this.props.value, nextProps.value)) {
      this.setState({ value: nextProps.value })
    }
  }

  handleClick (value) {
    this.setState({ value, expanded: false })
    if (this.props.handleChange) { this.props.handleChange(value) }
  }

  handleChange (event) {
    this.setState({ search: event.target.value })
  }

  handleBlur () {
    this.setState({ expanded: false })
    if (this.props.handleBlur) { this.props.handleBlur() }
    if (this.props.handleChange) { this.props.handleChange(this.state.value) }
  }

  handleFocus () {
    this.setState({ expanded: true })
    if (this.props.handleFocus) { this.props.handleFocus() }
  }

  filter (search, items) {
    return !isNil(search) ? filter(x => contains(toUpper(search), toUpper(x.text)), items) : items
  }

  getText (value, items) {
    if (isNil(value)) return this.props.label
    return prop('text', head(filter(x => equals(x.value, value), items))) || this.props.label
  }

  render () {
    const { items, searchEnabled, borderColor } = this.props
    const { value, search, expanded } = this.state

    return (
      <SelectInput
        items={this.filter(search, items)}
        display={this.getText(value, items)}
        expanded={expanded}
        searchEnabled={searchEnabled}
        borderColor={borderColor}
        value={value}
        handleBlur={this.handleBlur}
        handleChange={this.handleChange}
        handleClick={this.handleClick}
        handleFocus={this.handleFocus}
      />
    )
  }
}

SelectInputContainer.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    text: PropTypes.oneOfType([PropTypes.string.isRequired, PropTypes.object.isRequired]).isRequired,
    value: PropTypes.oneOfType([PropTypes.string.isRequired, PropTypes.number.isRequired, PropTypes.object.isRequired])
  })).isRequired,
  label: PropTypes.string,
  searchEnabled: PropTypes.bool,
  borderColor: PropTypes.oneOf(keysIn(Palette)),
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.object]),
  handleChange: PropTypes.func,
  handleBlur: PropTypes.func,
  handleFocus: PropTypes.func
}

SelectInputContainer.defaultProps = {
  label: 'Select a value',
  searchEnabled: false,
  borderColor: 'gray-2'
}

export default SelectInputContainer
