import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { keysIn } from 'ramda'

import { Color, Palette } from '../../Colors'
import { Icon } from '../../Icons'

const Input = styled.div`
  position: relative;
  display: block;
  width: 100%;
  background-color: ${props => Color('white')};
`
const Display = styled.div`
  border: 1px solid ${props => Color(props.borderColor)};
`
const Button = styled.button.attrs({
  type: 'button'
})`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  height: 40px;
  padding: 4px 8px;
  boox-sizing: border-box;
  transition: all 0.2s ease-in-out;
  background-color: ${props => Color('white')};
  color: ${props => Color('gray-4')};
  font-family: 'Montserrat', sans-serif !important;
  font-size: 14px;
  font-weight: 300;
  cursor: pointer;
  border: none;
  white-space: nowrap;
  user-select: none;

  &:focus { outline: none; }
`
const Search = styled.input.attrs({ type: 'text' })`
  width: 100%;
  height: 40px;
  padding: 4px 8px;
  box-sizing: border-box;
  background-color: ${props => Color('white')};
  color: ${props => Color('gray-3')};
  font-size: 14px;
  font-weight: normal;
  border: none;
  outline: none;
  box-shadow: none;

  &:focus {
    border-radius: none;
    border: none
    outline: none;
  }
`
const List = styled.div`
  position: absolute;
  display: ${props => props.expanded ? 'flex' : 'none'};
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;
  height: auto;
  max-height: 140px;
  overflow-y: auto;
  overflow-x: hidden;
  background-color: ${props => Color('white')};
  border: 1px solid ${props => Color('gray-2')};
  box-sizing: border-box;
  z-index: 10;
`
const ListItem = styled.a`
  width: 100%;
  padding: 8px 16px;
  box-sizing: border-box;
  font-family: 'Montserrat', sans-serif !important;
  font-size: 14px;
  font-weight: 300;
  color: ${props => Color('gray-4')};
  cursor: pointer;

  &:hover {
    color: ${props => Color('gray-4')};
    background-color: ${props => Color('gray-1')};
  }
`
const Arrow = styled.div`
  position: absolute;
  top: 30%;
  right: 15px;
`

const SelectInput = (props) => {
  const { items, display, expanded, searchEnabled, borderColor, handleBlur, handleChange, handleClick, handleFocus } = props

  return (
    <Input onBlur={handleBlur} onFocus={handleFocus}>
      <Display borderColor={borderColor}>
        {expanded && searchEnabled
          ? <Search autoFocus={expanded} onChange={handleChange} />
          : <Button>{display}</Button>
        }
      </Display>
      <Arrow>
        <Icon name='caret-down' size='16px' />
      </Arrow>
      <List expanded={expanded}>
        {items.map((item, index) => <ListItem key={index} onMouseDown={() => handleClick(item.value)}>{item.text}</ListItem>)}
      </List>
    </Input>
  )
}

SelectInput.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    text: PropTypes.string.isRequired,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.object])
  })).isRequired,
  display: PropTypes.string.isRequired,
  expanded: PropTypes.bool.isRequired,
  borderColor: PropTypes.oneOf(keysIn(Palette)),
  searchEnabled: PropTypes.bool.isRequired,
  handleBlur: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleClick: PropTypes.func.isRequired,
  handleFocus: PropTypes.func.isRequired
}

SelectInput.defaultProps = {
  borderColor: 'gray-2'
}

export default SelectInput
