import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { keysIn } from 'ramda'

import { Color, Palette } from '../../Colors'
import { Icon } from '../../Icons'
import { Markdown } from '../../Markdown'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;  
  width: 100%;
  height: auto;
  border: 1px solid ${props => Color(props.borderColor)};
  border-box: box-sizing;
 `
const Menu = styled.div`
  display: ${props => props.displayed ? 'flex' : 'none'};
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  width: 100%;
  height: 30px;
  border: none;
  box-sizing: border-box;
`
const MenuButton = styled(Icon).attrs({ cursor: true })`
  margin-right: 5px;
  margin-left: 5px;
  box-sizing: border-box;
`
const MenuButtonPreview = MenuButton.extend.attrs({ name: 'code', size: '20px', color: 'gray-5' })`
  &:hover { color: ${Color('red')}; }
`
const Editor = styled.div`
  display: ${props => props.displayed ? 'block' : 'none'};
  width: 100%;
`
const TextArea = styled.textarea`
  display: block;
  width: 100%;
  height: 300px;
  padding: 5px;
  box-sizing: border-box;
  border: none;
  outline: none;
  resize: none;
`
const Preview = styled.div`
  display: ${props => props.displayed ? 'block' : 'none'};
  width: 100%;
  height: 300px;
  overflow-y: scroll;
`

const MarkdownInput = props => {
  const { previewMode, previewModeEnabled, borderColor, handleChange, handleToggle, value } = props

  return (
    <Wrapper borderColor={borderColor}>
      <Menu displayed={previewModeEnabled}>
        <MenuButtonPreview onClick={handleToggle} />
      </Menu>
      <Editor displayed={!previewMode}>
        <TextArea onChange={handleChange} value={value} />
      </Editor>
      <Preview displayed={previewMode && previewModeEnabled}>
        <Markdown content={value} />
      </Preview>
    </Wrapper>
  )
}

MarkdownInput.propTypes = {
  previewModeEnabled: PropTypes.bool.isRequired,
  borderColor: PropTypes.oneOf(keysIn(Palette))
}

MarkdownInput.defaultProps = {
  borderColor: 'gray-2'
}

export default MarkdownInput
