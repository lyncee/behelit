import React from 'react'
import PropTypes from 'prop-types'
import { keysIn } from 'ramda'

import { Palette } from '../../Colors'
import MarkdownInput from './template.js'

class MarkdownInputContainer extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      previewMode: false,
      value: props.value
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleToggle = this.handleToggle.bind(this)
  }

  handleChange (e) {
    const value = e.target.value
    this.setState({ value })
    if (this.props.onChange) { this.props.onChange(value) }
  }

  handleToggle () {
    this.setState({ previewMode: !this.state.previewMode })
  }

  render () {
    const { previewModeEnabled, borderColor } = this.props

    return (
      <MarkdownInput
        previewMode={this.state.previewMode}
        previewModeEnabled={previewModeEnabled}
        borderColor={borderColor}
        value={this.state.value}
        handleChange={this.handleChange}
        handleToggle={this.handleToggle}
      />
    )
  }
}

MarkdownInputContainer.propTypes = {
  previewModeEnabled: PropTypes.bool,
  borderColor: PropTypes.oneOf(keysIn(Palette)),
  value: PropTypes.string,
  onChange: PropTypes.func
}

MarkdownInputContainer.defaultProps = {
  previewModeEnabled: false,
  borderColor: 'gray-2'
}

export default MarkdownInputContainer
