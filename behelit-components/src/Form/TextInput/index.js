import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { keysIn } from 'ramda'

import { Color, Palette } from '../../Colors'

const Input = styled.input.attrs({
  type: 'text'
})`
  display: block;
  width: 100%;
  height: 40px;
  min-height: 40px;
  padding: 6px 12px;
  box-sizing: border-box;
  font-size: 14px;
  font-weight: 300;
  line-height: 1.42;
  color: ${Color('gray-5')};
  background-color: ${Color('white')};
  background-image: none;
  outline-width: 0;
  user-select: text;
  border: 1px solid  ${props => Color(props.borderColor)};
  &::-webkit-input-placeholder { color: ${Color('gray-2')}; }
`

const TextInput = props => <Input {...props} />

TextInput.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func,
  borderColor: PropTypes.oneOf(keysIn(Palette))
}

TextInput.defaultProps = {
  borderColor: 'gray-2'
}

export default TextInput
