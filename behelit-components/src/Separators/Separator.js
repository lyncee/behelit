import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { Color } from '../Colors'

const Wrapper = styled.div`
  display: flex;
  fle-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 100%;
`
const Content = styled.div`
  white-space: nowrap;
  padding-left: ${props => props.align === 'left' || props.align === 'center' ? '10px' : '0'};
  padding-right: ${props => props.align === 'right' || props.align === 'center' ? '10px' : '0'}; 
`
const BaseSeparator = styled.div`
  width: 100%;
  height: 1px;
  background-color: ${props => Color(props.color)};
  margin: 10px 0;
`

const Separator = props => {
  const { children, align, color } = props

  return children
    ? (
      <Wrapper>
        {align !== 'right' && <BaseSeparator color={color} />}
        <Content align={align}>{children}</Content>
        {align !== 'left' && <BaseSeparator color={color} />}
      </Wrapper>
    ) : (
      <BaseSeparator color={color} />
    )
}

Separator.propTypes = {
  align: PropTypes.oneOf(['left', 'right', 'center']),
  color: PropTypes.string
}

Separator.defaultProps = {
  align: 'center',
  color: 'gray-6'
}

export default Separator
