import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

import { Color } from '../Colors'

const BaseText = styled(({ tag, children, ...props }) => React.createElement(tag, props, children))`
  color: ${props => Color(props.color)};
  font-family: ${props => props.font === 'cookie' ? "'Cookie'" : "'Roboto', sans serif"};
  font-weight: ${props => props.weight};
  font-size: ${props => props.size};
  text-transform: ${props => props.uppercase === 1 ? 'uppercase' : props.capitalize === 1 ? 'capitalize' : 'none'};
  text-decoration: ${props => props.underline === 1 ? 'underline' : 'none'};
  font-style: ${props => props.italic === 1 ? 'italic' : 'normal'};
  transition: 0.3s;
`

const Text = props => {
  const { children, altFont, uppercase, capitalize, underline, italic, ...rest } = props

  return (
    <BaseText
      {...rest}
      font={altFont ? 'cookie' : 'roboto'}
      uppercase={uppercase ? 1 : 0}
      capitalize={capitalize ? 1 : 0}
      underline={underline ? 1 : 0}
      italic={italic ? 1 : 0}
    >
      {children}
    </BaseText>
  )
}

Text.propTypes = {
  tag: PropTypes.string,
  altFont: PropTypes.bool,
  color: PropTypes.string,
  weight: PropTypes.oneOf([100, 300, 400, 500, 700, 900]),
  size: PropTypes.string,
  uppercase: PropTypes.bool,
  capitalize: PropTypes.bool,
  italic: PropTypes.bool,
  underline: PropTypes.bool
}

Text.defaultProps = {
  tag: 'span',
  altFont: false,
  color: 'gray-6',
  weight: 400,
  size: '16px',
  uppercase: false,
  capitalize: false,
  italic: false,
  underline: false
}

export default Text
