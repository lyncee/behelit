import React from 'react'
import styled from 'styled-components'

import { Color } from '../Colors'
import { Text } from '../Typography'

const BaseListItem = styled.li`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;
`
const BulletPoint = styled.div`
  display: flex;
  flex-shrink: 0;
  width: 5px;
  height: 5px;
  border: 2px solid ${Color('brand-primary')};
  border-radius: 50%;
  margin-right: 10px;
  margin-top: 5px;
`

const ListItem = props => {
  return (
    <BaseListItem>
      <BulletPoint />
      <Text size='16px' weight={300} color='gray-6'>{props.children}</Text>
    </BaseListItem>
  )
}

export default ListItem
