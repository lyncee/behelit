import React from 'react'
import styled from 'styled-components'

const BaseList = styled.ul`
  list-style-type: none;
  margin: 0;
  padding-left: 10px;
`

const List = (props) => {
  return (
    <BaseList>
      {props.children}
    </BaseList>
  )
}

export default List
