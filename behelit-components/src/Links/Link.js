import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { Color } from '../Colors'

const BaseLink = styled.a`
  color: ${props => Color(props.color)};
  font-family: 'Roboto', sans-serif;
  font-weight: ${props => props.weight};
  font-size: ${props => props.size};
  font-style: ${props => props.italic ? 'italic' : 'normal'};
  text-transform: ${props =>
    props.uppercase ? 'uppercase'
    : props.capitalize ? 'capitalize' : 'none'};
  text-decoration: none;
  cursor: pointer;

  &:hover { color: ${props => Color(props.color)}; }
  &:visited { color: ${props => Color(props.color)}; }
`

const Link = (props) => {
  const { children, ...rest } = props

  return (
    <BaseLink {...rest}>
      {children}
    </BaseLink>
  )
}

Link.propTypes = {
  color: PropTypes.string,
  weight: PropTypes.oneOf([100, 300, 400, 500, 700, 900]),
  size: PropTypes.string,
  uppercase: PropTypes.bool,
  capitalize: PropTypes.bool,
  italic: PropTypes.bool
}

Link.defaultProps = {
  color: 'dark-cyan',
  weight: 400,
  size: '16px',
  uppercase: false,
  capitalize: false,
  italic: false
}

export default Link
