
const emailRegex = new RegExp(/^(([^<>()\\[\]\\.,;:\s@"]+(\.[^<>()\\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)

const isRequired = value => value ? undefined : 'Required'

const isRequiredEmail = value => emailRegex.test(value) ? undefined : 'Invalid email address'

const getState = (meta) => ({
  errorState: !meta.touched ? 'initial' : (meta.invalid ? 'invalid' : 'valid'),
  errorMessage: meta.error
})

const getColor = (state) => {
  switch (state) {
    case 'initial': return 'gray-3'
    case 'invalid': return 'red'
    case 'valid': return 'green'
    default: return 'gray-3'
  }
}

export { isRequired, isRequiredEmail, getState, getColor }
