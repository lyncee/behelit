import { isNil } from 'ramda'

// checkStatus :: Response -> Promise Response
const checkStatus = (r) => r.status === 200 ? Promise.resolve(r) : r.json().then(j => { throw new Error(j.message) })

// extractData :: Response -> Promise (JSON | BLOB | TEXT)
const extractData = (r) => {
  const responseOfType = (t) => r.headers.get('content-type') && r.headers.get('content-type').indexOf(t) > -1
  switch (true) {
    case responseOfType('application/json'): return r.json()
    case responseOfType('image/jpeg'): return r.blob()
    default: return r.text()
  }
}

// Generic request object
const request = ({ method, url, endPoint, data, token }) => {
  const headers = !isNil(token)
    ? { 'Content-Type': 'application/json', 'x-access-token': token }
    : { 'Content-Type': 'application/json' }

  const finalOptions = method === 'GET' || method === 'DELETE' ? {
    method,
    headers
  } : {
    method,
    headers,
    body: JSON.stringify(data)
  }

  const finalUrl = `${url}/${endPoint}`

  return fetch(finalUrl, finalOptions).then(checkStatus).then(extractData)
}

// Get request
const get = ({ url, endPoint, data, token }) => request({ method: 'GET', url, endPoint, token })

// Post request
const post = ({ url, endPoint, data, token }) => request({ method: 'POST', url, endPoint, data, token })

// Put request
const put = ({ url, endPoint, data, token }) => request({ method: 'PUT', url, endPoint, data, token })

// Delete request
const del = ({ url, endPoint, data, token }) => request({ method: 'DELETE', url, endPoint, token })

export {
  get,
  post,
  put,
  del
}
