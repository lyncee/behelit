import config from 'config'
import { get, post } from '../fetch'

const url = config.apiUrl

const createComment = comment => post({
  url,
  endPoint: 'comments',
  data: comment
})

const createReview = review => post({
  url,
  endPoint: 'reviews',
  data: review
})

const getArticle = (id) => get({
  url,
  endPoint: `articles/${id}`
})

const getArticles = () => get({
  url,
  endPoint: 'articles'
})

const getComments = (articleId) => get({
  url,
  endPoint: `comments/${articleId}`
})

const getProjects = () => get({
  url,
  endPoint: 'projects'
})

const getReviews = () => get({
  url,
  endPoint: 'reviews'
})

const login = (username, password) => post({
  url,
  endPoint: 'token',
  data: { username, password }
})

const sendEmail = email => post({
  url,
  endPoint: 'contact',
  data: email
})

export default {
  createComment,
  createReview,
  getArticle,
  getArticles,
  getComments,
  getProjects,
  getReviews,
  login,
  sendEmail
}
