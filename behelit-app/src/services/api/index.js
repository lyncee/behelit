import publicApi from './publicApi'
import privateApi from './privateApi'

export default {
  public: publicApi,
  private: privateApi
}
