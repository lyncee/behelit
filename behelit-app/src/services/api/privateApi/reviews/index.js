const reviews = ({ url, get, post, put, del }) => {
  const createReview = (review, token) => post({
    url,
    endPoint: 'reviews',
    data: review,
    token
  })

  const deleteReview = (id, token) => del({
    url,
    endPoint: `reviews/${id}`,
    token
  })

  const editReview = (review, token) => put({
    url,
    endPoint: 'reviews',
    data: review,
    token
  })

  const getReviews = (token) => get({
    url,
    endPoint: 'reviews',
    token
  })

  const publishReview = (review, token) => post({
    url,
    endPoint: 'reviews/publish',
    data: review,
    token
  })

  const unpublishReview = (review, token) => post({
    url,
    endPoint: 'reviews/unpublish',
    data: review,
    token
  })

  return {
    createReview,
    deleteReview,
    editReview,
    getReviews,
    publishReview,
    unpublishReview
  }
}

export default reviews
