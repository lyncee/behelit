const users = ({ url, get, post, put, del }) => {
  const createUser = (user, token) => post({
    url,
    endPoint: 'users',
    data: user,
    token
  })

  const deleteUser = (id, token) => del({
    url,
    endPoint: `users/${id}`,
    token
  })

  const editUser = (user, token) => put({
    url,
    endPoint: 'users',
    data: user,
    token
  })

  const getUsers = (token) => get({
    url,
    endPoint: 'users',
    token
  })

  return {
    createUser,
    deleteUser,
    editUser,
    getUsers
  }
}

export default users
