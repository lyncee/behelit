import config from 'config'
import { get, post, put, del } from '../fetch'
import { mergeAll } from 'ramda'
import articles from './articles'
import comments from './comments'
import projects from './projects'
import reviews from './reviews'
import tags from './tags'
import users from './users'

const url = `${config.apiUrl}/admin`

export default mergeAll([
  articles({ url, get, post, put, del }),
  comments({ url, get, post, put, del }),
  projects({ url, get, post, put, del }),
  reviews({ url, get, post, put, del }),
  tags({ url, get, post, put, del }),
  users({ url, get, post, put, del })
])
