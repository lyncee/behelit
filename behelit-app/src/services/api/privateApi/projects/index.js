
const projects = ({ url, get, post, put, del }) => {
  const createProject = (project, token) => post({
    url,
    endPoint: 'projects',
    data: project,
    token
  })

  const deleteProject = (id, token) => del({
    url,
    endPoint: `projects/${id}`,
    token
  })

  const editProject = (project, token) => put({
    url,
    endPoint: 'projects',
    data: project,
    token
  })

  const getProjects = (token) => get({
    url,
    endPoint: 'projects',
    token
  })

  const publishProject = (project, token) => post({
    url,
    endPoint: 'projects/publish',
    data: project,
    token
  })

  const unpublishProject = (project, token) => post({
    url,
    endPoint: 'projects/unpublish',
    data: project,
    token
  })

  return {
    createProject,
    deleteProject,
    editProject,
    getProjects,
    publishProject,
    unpublishProject
  }
}

export default projects
