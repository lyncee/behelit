const tags = ({ url, get, post, put, del }) => {
  const createTag = (tag, token) => post({
    url,
    endPoint: 'tags',
    data: tag,
    token
  })

  const deleteTag = (id, token) => del({
    url,
    endPoint: `tags/${id}`,
    token
  })

  const editTag = (tag, token) => put({
    url,
    endPoint: 'tags',
    data: tag,
    token
  })

  const getTags = (token) => get({
    url,
    endPoint: 'tags',
    token
  })

  return {
    createTag,
    deleteTag,
    editTag,
    getTags
  }
}

export default tags
