
const articles = ({ url, get, post, put, del }) => {
  const createArticle = (article, token) => post({
    url,
    endPoint: 'articles',
    data: article,
    token
  })

  const deleteArticle = (id, token) => del({
    url,
    endPoint: `articles/${id}`,
    token
  })

  const editArticle = (article, token) => put({
    url,
    endPoint: 'articles',
    data: article,
    token
  })

  const getArticles = (token) => get({
    url,
    endPoint: 'articles',
    token
  })

  const publishArticle = (article, token) => post({
    url,
    endPoint: 'articles/publish',
    data: article,
    token
  })

  const unpublishArticle = (article, token) => post({
    url,
    endPoint: 'articles/unpublish',
    data: article,
    token
  })

  return {
    createArticle,
    deleteArticle,
    editArticle,
    getArticles,
    publishArticle,
    unpublishArticle
  }
}

export default articles
