
const comments = ({ url, get, post, put, del }) => {
  const deleteComment = (id, token) => del({
    url,
    endPoint: `comments/${id}`,
    token
  })

  const getComments = (token) => get({
    url,
    endPoint: 'comments',
    token
  })

  const publishComment = (comment, token) => post({
    url,
    endPoint: 'comments/publish',
    data: comment,
    token
  })

  const unpublishComment = (comment, token) => post({
    url,
    endPoint: 'comments/unpublish',
    data: comment,
    token
  })

  return {
    deleteComment,
    getComments,
    publishComment,
    unpublishComment
  }
}

export default comments
