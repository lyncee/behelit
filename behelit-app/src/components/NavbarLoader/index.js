import React from 'react'
import { connect } from 'react-redux'
import { selectors } from 'data'

import { Spinner } from 'behelit-components'

class LoaderContainer extends React.Component {
  render () {
    return this.props.isFetching ? <Spinner /> : <div />
  }
}

const mapStateToProps = (state, ownProps) => ({
  isFetching: selectors.application.selectIsFetching(state)
})

export default connect(mapStateToProps)(LoaderContainer)
