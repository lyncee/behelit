import React from 'react'
import styled from 'styled-components'

import { getState, getColor } from 'services/form'
import { Color, SelectInput } from 'behelit-components'

const Wrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;  
  width: 100%;
  height: auto;
 `
const Error = styled.span`
  position: absolute;
  display: block;
  top: -18px;
  right: 0;
  height: 15px;
  font-family: 'Roboto', sans-serif;
  font-size: 13px;
  font-weight: 300;
  color: ${Color('red')};
`

const SelectBox = props => {
  const { input, meta, ...rest } = props
  const { value, onChange, onBlur, onFocus } = input
  const { errorState, errorMessage } = getState(meta)
  const color = getColor(errorState)

  return (
    <Wrapper>
      <SelectInput borderColor={color} value={value} handleChange={onChange} handleBlur={onBlur} handleFocus={onFocus} {...rest} />
      { errorState === 'invalid' && <Error>{errorMessage}</Error> }
    </Wrapper>
  )
}

export default SelectBox
