import React from 'react'
import styled from 'styled-components'

import { getState, getColor } from 'services/form'
import { Color, MarkdownInput } from 'behelit-components'

const Wrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;  
  width: 100%;
  height: auto;
 `
const Error = styled.span`
  position: absolute;
  display: block;
  top: -18px;
  right: 0;
  height: 15px;
  font-family: 'Roboto', sans-serif;
  font-size: 13px;
  font-weight: 300;
  color: ${Color('red')};
`

const MarkdownBox = props => {
  const { input, meta, ...rest } = props
  const { errorState, errorMessage } = getState(meta)
  const color = getColor(errorState)

  return (
    <Wrapper>
      <MarkdownInput borderColor={color} {...input} {...rest} />
      { errorState === 'invalid' && <Error>{errorMessage}</Error> }
    </Wrapper>
  )
}

export default MarkdownBox
