import styled from 'styled-components'
import { Form } from 'redux-form'

export default styled(Form)`
  & > * { margin-bottom: 15px; }
`
