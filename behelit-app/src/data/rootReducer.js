import { combineReducers } from 'redux'
import admin from './admin/reducers'
import alerts from './alerts/reducers'
import application from './application/reducers'
import articles from './articles/reducers'
import auth from './auth/reducers'
import comments from './comments/reducers'
import { reducer as formReducer } from 'redux-form'
import projects from './projects/reducers'
import reviews from './reviews/reducers'
import { reducer as reduxUiReducer } from 'redux-ui'

const rootReducer = combineReducers({
  admin,
  alerts,
  application,
  articles,
  auth,
  comments,
  form: formReducer,
  projects,
  reviews,
  ui: reduxUiReducer
})

export default rootReducer
