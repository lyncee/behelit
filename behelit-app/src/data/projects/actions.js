import * as AT from './actionTypes'

export const fetchProjects = () => ({ type: AT.FETCH_PROJECTS })

export const setProjects = (projects) => ({ type: AT.SET_PROJECTS, payload: { projects } })
