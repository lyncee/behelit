import { takeLatest, put, call } from 'redux-saga/effects'
import { application, alerts } from '../actions'
import * as A from './actions'
import * as AT from './actionTypes'
import api from 'services/api'

export const fetchProjects = function * () {
  try {
    yield put(application.startRequest())
    const response = yield call(api.public.getProjects)
    yield put(A.setProjects(response))
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export default function * () {
  yield takeLatest(AT.FETCH_PROJECTS, fetchProjects)
}
