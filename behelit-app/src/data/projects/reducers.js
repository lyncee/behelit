import * as AT from './actionTypes'

const INITIAL_STATE = []

const projects = (state = INITIAL_STATE, action) => {
  const { type, payload } = action

  switch (type) {
    case AT.SET_PROJECTS: {
      const { projects } = payload
      return projects
    }

    default: {
      return state
    }
  }
}

export default projects
