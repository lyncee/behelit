import * as admin from './admin/selectors'
import * as alerts from './alerts/selectors'
import * as application from './application/selectors'
import * as articles from './articles/selectors'
import * as auth from './auth/selectors'
import * as comments from './comments/selectors'
import * as projects from './projects/selectors'
import * as reviews from './reviews/selectors'

export {
  admin,
  alerts,
  application,
  articles,
  auth,
  comments,
  projects,
  reviews
}
