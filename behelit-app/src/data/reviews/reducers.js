import * as AT from './actionTypes'

const INITIAL_STATE = []

const reviews = (state = INITIAL_STATE, action) => {
  const { type, payload } = action

  switch (type) {
    case AT.SET_REVIEWS: {
      const { reviews } = payload
      return Array.from(reviews)
    }
    default: {
      return state
    }
  }
}

export default reviews
