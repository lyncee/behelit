import { takeLatest, call, put } from 'redux-saga/effects'
import { application, alerts } from '../actions'
import * as A from './actions'
import * as AT from './actionTypes'
import api from 'services/api'

export const createReview = function * (action) {
  try {
    const review = action.payload
    yield put(application.startRequest())
    yield call(api.public.createReview, review)
    yield put(alerts.displaySuccess('Your review has been successfully submitted and will be published shortly.'))
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export const fetchReviews = function * () {
  try {
    yield put(application.startRequest())
    const response = yield call(api.public.getReviews)
    yield put(A.setReviews(response))
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export default function * () {
  yield takeLatest(AT.CREATE_REVIEW, createReview)
  yield takeLatest(AT.FETCH_REVIEWS, fetchReviews)
}
