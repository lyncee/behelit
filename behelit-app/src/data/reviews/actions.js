import * as AT from './actionTypes'

export const createReview = (name, company, role, message) => ({ type: AT.CREATE_REVIEW, payload: { name, company, role, message } })

export const fetchReviews = () => ({ type: AT.FETCH_REVIEWS })

export const setReviews = (reviews) => ({ type: AT.SET_REVIEWS, payload: { reviews } })
