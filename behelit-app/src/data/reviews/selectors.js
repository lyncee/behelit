import { path } from 'ramda'

export const selectReviews = path(['reviews'])
