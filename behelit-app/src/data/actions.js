import * as admin from './admin/actions'
import * as alerts from './alerts/actions'
import * as application from './application/actions'
import * as articles from './articles/actions'
import * as auth from './auth/actions'
import * as comments from './comments/actions'
import { actions as form } from 'redux-form'
import * as projects from './projects/actions'
import * as reviews from './reviews/actions'
import { routerActions as router } from 'connected-react-router'

export {
  admin,
  alerts,
  application,
  articles,
  auth,
  comments,
  form,
  projects,
  reviews,
  router
}
