import { path } from 'ramda'

export const selectIsFetching = path(['application', 'isFetching'])
