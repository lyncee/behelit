import * as AT from './actionTypes'

const INITIAL_STATE = {
  isLoading: true,
  isFetching: false
}

const application = (state = INITIAL_STATE, action) => {
  const { type } = action

  switch (type) {
    case AT.START_REQUEST: {
      return Object.assign({}, INITIAL_STATE, { isFetching: true })
    }
    case AT.STOP_REQUEST: {
      return Object.assign({}, INITIAL_STATE, { isFetching: false })
    }
    default: {
      return state
    }
  }
}

export default application
