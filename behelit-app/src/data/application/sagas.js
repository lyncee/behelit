import { put, call, takeLatest } from 'redux-saga/effects'
import { alerts } from '../actions'
import * as A from './actions'
import * as AT from './actionTypes'
import api from 'services/api'

export const sendEmail = function * (action) {
  try {
    const { email } = action.payload
    yield put(A.startRequest())
    yield call(api.public.sendEmail, email)
    yield put(alerts.displaySuccess('Your email has been sent successfully.'))
  } catch (e) {
    yield put(alerts.displaySuccess('Your email could not be sent.'))
  } finally {
    yield put(A.stopRequest())
  }
}

export default function * () {
  yield takeLatest(AT.SEND_EMAIL, sendEmail)
}
