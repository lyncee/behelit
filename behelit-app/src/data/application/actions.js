import * as AT from './actionTypes'

export const startRequest = () => ({ type: AT.START_REQUEST })

export const stopRequest = () => ({ type: AT.STOP_REQUEST })

export const startTransition = () => ({ type: AT.START_TRANSITION })

export const stopTransition = () => ({ type: AT.STOP_TRANSITION })

export const sendEmail = (email) => ({ type: AT.SEND_EMAIL, payload: { email } })
