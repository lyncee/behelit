import { delay } from 'redux-saga'
import { takeEvery, call, put } from 'redux-saga/effects'

import { alerts } from '../actions'
import * as AT from './actionTypes'

const DISMISS_AFTER = 7000

const handleTimer = function * (action) {
  const { id } = action.payload
  yield call(delay, DISMISS_AFTER)
  yield put(alerts.dismissAlert(id))
}

export default function * sagas () {
  yield takeEvery(AT.ALERTS_SHOW, handleTimer)
}
