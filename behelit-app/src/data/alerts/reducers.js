import { prepend, filter } from 'ramda'
import * as AT from './actionTypes'

const INITIAL_STATE = []

const alerts = (state = INITIAL_STATE, action) => {
  const { type, payload } = action

  switch (type) {
    case AT.ALERTS_CLEAR: {
      return INITIAL_STATE
    }
    case AT.ALERTS_DISMISS: {
      const { id } = payload
      return filter(a => a.id !== id, state)
    }
    case AT.ALERTS_SHOW: {
      const { id, nature, message, data } = action.payload
      return prepend({ id, nature, message, data }, state)
    }
    default: {
      return state
    }
  }
}

export default alerts
