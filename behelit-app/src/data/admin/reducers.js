import { combineReducers } from 'redux'
import articles from './articles/reducers.js'
import comments from './comments/reducers.js'
import projects from './projects/reducers.js'
import reviews from './reviews/reducers.js'
import tags from './tags/reducers.js'
import users from './users/reducers.js'

const admin = combineReducers({
  articles,
  comments,
  projects,
  reviews,
  tags,
  users
})

export default admin
