import * as AT from './actionTypes'

export const createTag = (title) => ({ type: AT.ADMIN_CREATE_TAG, payload: { title } })

export const deleteTag = (id) => ({ type: AT.ADMIN_DELETE_TAG, payload: { id } })

export const editTag = (id, title) => ({ type: AT.ADMIN_EDIT_TAG, payload: { id, title } })

export const fetchTags = () => ({ type: AT.ADMIN_FETCH_TAGS })

export const setTags = (tags) => ({ type: AT.ADMIN_SET_TAGS, payload: { tags } })
