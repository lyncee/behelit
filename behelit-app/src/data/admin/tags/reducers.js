import * as AT from './actionTypes'

const INITIAL_STATE = []

const tag = (state = INITIAL_STATE, action) => {
  const { type, payload } = action

  switch (type) {
    case AT.ADMIN_SET_TAGS: {
      return payload.tags
    }
    default: {
      return state
    }
  }
}

export default tag
