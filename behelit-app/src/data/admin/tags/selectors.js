import { compose, filter, head, path } from 'ramda'

export const selectTag = id => compose(head, filter(x => x._id === id), selectTags)

export const selectTags = path(['admin', 'tags'])
