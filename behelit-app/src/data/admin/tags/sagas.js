import { takeLatest, call, put, select } from 'redux-saga/effects'
import { application, alerts, router } from '../../actions'
import { selectToken } from '../../auth/selectors'
import * as A from './actions'
import * as AT from './actionTypes'
import api from 'services/api'

export const createTag = function * (action) {
  try {
    const token = yield select(selectToken)
    yield put(application.startRequest())
    yield call(api.private.createTag, action.payload, token)
    yield put(router.push('/admin/tags'))
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export const deleteTag = function * (action) {
  try {
    const token = yield select(selectToken)
    const { id } = action.payload
    yield put(application.startRequest())
    yield call(api.private.deleteTag, id, token)
    yield call(fetchTags)
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export const editTag = function * (action) {
  try {
    const token = yield select(selectToken)
    yield put(application.startRequest())
    yield call(api.private.editTag, action.payload, token)
    yield put(router.push('/admin/tags'))
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export const fetchTags = function * () {
  try {
    const token = yield select(selectToken)
    yield put(application.startRequest())
    const response = yield call(api.private.getTags, token)
    yield put(A.setTags(response))
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export default function * () {
  yield takeLatest(AT.ADMIN_CREATE_TAG, createTag)
  yield takeLatest(AT.ADMIN_DELETE_TAG, deleteTag)
  yield takeLatest(AT.ADMIN_EDIT_TAG, editTag)
  yield takeLatest(AT.ADMIN_FETCH_TAGS, fetchTags)
}
