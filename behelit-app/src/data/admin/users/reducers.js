import * as AT from './actionTypes'

const INITIAL_STATE = []

const user = (state = INITIAL_STATE, action) => {
  const { type, payload } = action

  switch (type) {
    case AT.ADMIN_SET_USERS: {
      return payload.users
    }
    default: {
      return state
    }
  }
}

export default user
