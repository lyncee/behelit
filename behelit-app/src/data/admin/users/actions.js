import * as AT from './actionTypes'

export const createUser = (username, email, password) => ({ type: AT.ADMIN_CREATE_USER, payload: { username, email, password } })

export const deleteUser = (id) => ({ type: AT.ADMIN_DELETE_USER, payload: { id } })

export const editUser = (id, username, email, isAdmin) => ({ type: AT.ADMIN_EDIT_USER, payload: { id, username, email, isAdmin } })

export const fetchUsers = () => ({ type: AT.ADMIN_FETCH_USERS })

export const setUsers = (users) => ({ type: AT.ADMIN_SET_USERS, payload: { users } })
