import { takeLatest, call, put, select } from 'redux-saga/effects'
import { application, alerts, router } from '../../actions'
import { selectToken } from '../../auth/selectors'
import * as A from './actions'
import * as AT from './actionTypes'
import api from 'services/api'

export const createUser = function * (action) {
  try {
    const token = yield select(selectToken)
    yield put(application.startRequest())
    yield call(api.private.createUser, action.payload, token)
    yield put(router.push('/admin/users'))
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export const deleteUser = function * (action) {
  try {
    const token = yield select(selectToken)
    const { id } = action.payload
    yield put(application.startRequest())
    yield call(api.private.deleteUser, id, token)
    yield call(fetchUsers)
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export const editUser = function * (action) {
  try {
    const token = yield select(selectToken)
    yield put(application.startRequest())
    yield call(api.private.editUser, action.payload, token)
    yield put(router.push('/admin/users'))
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export const fetchUsers = function * () {
  try {
    const token = yield select(selectToken)
    yield put(application.startRequest())
    const response = yield call(api.private.getUsers, token)
    yield put(A.setUsers(response))
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export default function * () {
  yield takeLatest(AT.ADMIN_CREATE_USER, createUser)
  yield takeLatest(AT.ADMIN_DELETE_USER, deleteUser)
  yield takeLatest(AT.ADMIN_EDIT_USER, editUser)
  yield takeLatest(AT.ADMIN_FETCH_USERS, fetchUsers)
}
