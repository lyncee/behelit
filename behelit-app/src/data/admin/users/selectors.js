
import { compose, filter, head, path } from 'ramda'

export const selectUser = id => compose(head, filter(x => x._id === id), selectUsers)

export const selectUsers = path(['admin', 'users'])
