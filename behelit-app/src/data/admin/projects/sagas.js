import { takeLatest, call, put, select } from 'redux-saga/effects'
import { application, alerts, router } from '../../actions'
import { selectToken } from '../../auth/selectors'
import * as A from './actions'
import * as AT from './actionTypes'
import api from 'services/api'

export const createProject = function * (action) {
  try {
    const token = yield select(selectToken)
    yield put(application.startRequest())
    yield call(api.private.createProject, action.payload, token)
    yield put(router.push('/admin/projects'))
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export const deleteProject = function * (action) {
  try {
    const token = yield select(selectToken)
    const { id } = action.payload
    yield put(application.startRequest())
    yield call(api.private.deleteProject, id, token)
    yield call(fetchProjects)
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export const editProject = function * (action) {
  try {
    const token = yield select(selectToken)
    yield put(application.startRequest())
    yield call(api.private.editProject, action.payload, token)
    yield put(router.push('/admin/projects'))
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export const fetchProjects = function * () {
  try {
    const token = yield select(selectToken)
    yield put(application.startRequest())
    const response = yield call(api.private.getProjects, token)
    yield put(A.setProjects(response))
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export const publishProject = function * (action) {
  try {
    const token = yield select(selectToken)
    yield put(application.startRequest())
    yield call(api.private.publishProject, action.payload, token)
    yield call(fetchProjects)
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export const unpublishProject = function * (action) {
  try {
    const token = yield select(selectToken)
    yield put(application.startRequest())
    yield call(api.private.unpublishProject, action.payload, token)
    yield call(fetchProjects)
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export default function * () {
  yield takeLatest(AT.ADMIN_CREATE_PROJECT, createProject)
  yield takeLatest(AT.ADMIN_DELETE_PROJECT, deleteProject)
  yield takeLatest(AT.ADMIN_EDIT_PROJECT, editProject)
  yield takeLatest(AT.ADMIN_FETCH_PROJECTS, fetchProjects)
  yield takeLatest(AT.ADMIN_PUBLISH_PROJECT, publishProject)
  yield takeLatest(AT.ADMIN_UNPUBLISH_PROJECT, unpublishProject)
}
