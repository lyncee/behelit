import * as AT from './actionTypes'

export const createProject = (title, summary, description, company, year, tags) => ({ type: AT.ADMIN_CREATE_PROJECT, payload: { title, summary, description, company, year, tags } })

export const deleteProject = (id) => ({ type: AT.ADMIN_DELETE_PROJECT, payload: { id } })

export const editProject = (id, title, summary, description, company, year, tags) => ({ type: AT.ADMIN_EDIT_PROJECT, payload: { id, title, summary, description, company, year, tags } })

export const fetchProjects = () => ({ type: AT.ADMIN_FETCH_PROJECTS })

export const setProjects = (projects) => ({ type: AT.ADMIN_SET_PROJECTS, payload: { projects } })

export const publishProject = (id) => ({ type: AT.ADMIN_PUBLISH_PROJECT, payload: { id } })

export const unpublishProject = (id) => ({ type: AT.ADMIN_UNPUBLISH_PROJECT, payload: { id } })
