import { compose, filter, head, path } from 'ramda'

export const selectProject = id => compose(head, filter(x => x._id === id), selectProjects)

export const selectProjects = path(['admin', 'projects'])
