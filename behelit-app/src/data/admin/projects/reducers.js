import * as AT from './actionTypes'

const INITIAL_STATE = []

const project = (state = INITIAL_STATE, action) => {
  const { type, payload } = action

  switch (type) {
    case AT.ADMIN_SET_PROJECTS: {
      return payload.projects
    }
    default: {
      return state
    }
  }
}

export default project
