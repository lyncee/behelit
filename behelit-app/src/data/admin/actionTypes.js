import * as articles from './articles/actionTypes'
import * as comments from './comments/actionTypes'
import * as projects from './projects/actionTypes'
import * as reviews from './reviews/actionTypes'
import * as tags from './tags/actionTypes'
import * as users from './users/actionTypes'

export {
  articles,
  comments,
  projects,
  reviews,
  tags,
  users
}
