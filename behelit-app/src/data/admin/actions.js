import * as articles from './articles/actions'
import * as comments from './comments/actions'
import * as projects from './projects/actions'
import * as reviews from './reviews/actions'
import * as tags from './tags/actions'
import * as users from './users/actions'

export {
  articles,
  comments,
  projects,
  reviews,
  tags,
  users
}
