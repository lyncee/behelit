import { all, fork } from 'redux-saga/effects'
import articles from './articles/sagas'
import comments from './comments/sagas'
import projects from './projects/sagas'
import reviews from './reviews/sagas'
import tags from './tags/sagas'
import users from './users/sagas'

const rootSaga = function * () {
  yield all([
    fork(articles),
    fork(comments),
    fork(projects),
    fork(reviews),
    fork(tags),
    fork(users)
  ])
}

export default rootSaga
