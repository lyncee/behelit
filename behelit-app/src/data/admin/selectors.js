import * as articles from './articles/selectors'
import * as comments from './comments/selectors'
import * as projects from './projects/selectors'
import * as reviews from './reviews/selectors'
import * as tags from './tags/selectors'
import * as users from './users/selectors'

export {
  articles,
  comments,
  projects,
  reviews,
  tags,
  users
}
