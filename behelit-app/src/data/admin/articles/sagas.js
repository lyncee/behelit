import { takeLatest, call, put, select } from 'redux-saga/effects'
import { application, alerts, router } from '../../actions'
import { selectToken } from '../../auth/selectors'
import * as A from './actions'
import * as AT from './actionTypes'
import api from 'services/api'

export const createArticle = function * (action) {
  try {
    const token = yield select(selectToken)
    yield put(application.startRequest())
    yield call(api.private.createArticle, action.payload, token)
    yield put(router.push('/admin/articles'))
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export const deleteArticle = function * (action) {
  try {
    const token = yield select(selectToken)
    const { id } = action.payload
    yield put(application.startRequest())
    yield call(api.private.deleteArticle, id, token)
    yield call(fetchArticles)
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export const editArticle = function * (action) {
  try {
    const token = yield select(selectToken)
    yield put(application.startRequest())
    yield call(api.private.editArticle, action.payload, token)
    yield put(router.push('/admin/articles'))
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export const fetchArticles = function * () {
  try {
    const token = yield select(selectToken)
    yield put(application.startRequest())
    const response = yield call(api.private.getArticles, token)
    yield put(A.setArticles(response))
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export const publishArticle = function * (action) {
  try {
    const token = yield select(selectToken)
    yield put(application.startRequest())
    yield call(api.private.publishArticle, action.payload, token)
    yield call(fetchArticles)
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export const unpublishArticle = function * (action) {
  try {
    const token = yield select(selectToken)
    yield put(application.startRequest())
    yield call(api.private.unpublishArticle, action.payload, token)
    yield call(fetchArticles)
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export default function * () {
  yield takeLatest(AT.ADMIN_CREATE_ARTICLE, createArticle)
  yield takeLatest(AT.ADMIN_DELETE_ARTICLE, deleteArticle)
  yield takeLatest(AT.ADMIN_EDIT_ARTICLE, editArticle)
  yield takeLatest(AT.ADMIN_FETCH_ARTICLES, fetchArticles)
  yield takeLatest(AT.ADMIN_PUBLISH_ARTICLE, publishArticle)
  yield takeLatest(AT.ADMIN_UNPUBLISH_ARTICLE, unpublishArticle)
}
