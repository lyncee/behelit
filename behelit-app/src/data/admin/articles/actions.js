import * as AT from './actionTypes'

export const createArticle = (title, description, content, author, category, tags) => ({ type: AT.ADMIN_CREATE_ARTICLE, payload: { title, description, content, author, category, tags } })

export const deleteArticle = (id) => ({ type: AT.ADMIN_DELETE_ARTICLE, payload: { id } })

export const editArticle = (id, title, description, content, author, category, tags) => ({ type: AT.ADMIN_EDIT_ARTICLE, payload: { id, title, description, content, author, category, tags } })

export const fetchArticles = () => ({ type: AT.ADMIN_FETCH_ARTICLES })

export const setArticles = (articles) => ({ type: AT.ADMIN_SET_ARTICLES, payload: { articles } })

export const publishArticle = (id) => ({ type: AT.ADMIN_PUBLISH_ARTICLE, payload: { id } })

export const unpublishArticle = (id) => ({ type: AT.ADMIN_UNPUBLISH_ARTICLE, payload: { id } })
