import * as AT from './actionTypes'

const INITIAL_STATE = []

const article = (state = INITIAL_STATE, action) => {
  const { type, payload } = action

  switch (type) {
    case AT.ADMIN_SET_ARTICLES: {
      return payload.articles
    }
    default: {
      return state
    }
  }
}

export default article
