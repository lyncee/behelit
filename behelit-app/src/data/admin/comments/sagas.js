import { takeLatest, call, put, select } from 'redux-saga/effects'
import { application, alerts, router } from '../../actions'
import { selectToken } from '../../auth/selectors'
import * as A from './actions'
import * as AT from './actionTypes'
import api from 'services/api'

export const deleteComment = function * (action) {
  try {
    const token = yield select(selectToken)
    const { id } = action.payload
    yield put(application.startRequest())
    yield call(api.private.deleteComment, id, token)
    yield call(fetchComments)
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export const fetchComments = function * () {
  try {
    const token = yield select(selectToken)
    yield put(application.startRequest())
    const response = yield call(api.private.getComments, token)
    yield put(A.setComments(response))
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export const publishComment = function * (action) {
  try {
    const token = yield select(selectToken)
    yield put(application.startRequest())
    yield call(api.private.publishComment, action.payload, token)
    yield call(fetchComments)
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export const unpublishComment = function * (action) {
  try {
    const token = yield select(selectToken)
    yield put(application.startRequest())
    yield call(api.private.unpublishComment, action.payload, token)
    yield call(fetchComments)
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export default function * () {
  yield takeLatest(AT.ADMIN_DELETE_COMMENT, deleteComment)
  yield takeLatest(AT.ADMIN_FETCH_COMMENTS, fetchComments)
  yield takeLatest(AT.ADMIN_PUBLISH_COMMENT, publishComment)
  yield takeLatest(AT.ADMIN_UNPUBLISH_COMMENT, unpublishComment)
}
