import * as AT from './actionTypes'

export const deleteComment = (id) => ({ type: AT.ADMIN_DELETE_COMMENT, payload: { id } })

export const fetchComments = () => ({ type: AT.ADMIN_FETCH_COMMENTS })

export const setComments = (comments) => ({ type: AT.ADMIN_SET_COMMENTS, payload: { comments } })

export const publishComment = (id) => ({ type: AT.ADMIN_PUBLISH_COMMENT, payload: { id } })

export const unpublishComment = (id) => ({ type: AT.ADMIN_UNPUBLISH_COMMENT, payload: { id } })
