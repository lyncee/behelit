import { path } from 'ramda'

export const selectComments = path(['admin', 'comments'])
