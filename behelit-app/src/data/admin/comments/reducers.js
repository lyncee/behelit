import * as AT from './actionTypes'

const INITIAL_STATE = []

const comment = (state = INITIAL_STATE, action) => {
  const { type, payload } = action

  switch (type) {
    case AT.ADMIN_SET_COMMENTS: {
      return payload.comments
    }
    default: {
      return state
    }
  }
}

export default comment
