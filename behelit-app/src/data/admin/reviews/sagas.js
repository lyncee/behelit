import { takeLatest, call, put, select } from 'redux-saga/effects'
import { application, alerts, router } from '../../actions'
import { selectToken } from '../../auth/selectors'
import * as A from './actions'
import * as AT from './actionTypes'
import api from 'services/api'

export const createReview = function * (action) {
  try {
    const token = yield select(selectToken)
    yield put(application.startRequest())
    yield call(api.private.createReview, action.payload, token)
    yield put(router.push('/admin/reviews'))
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export const deleteReview = function * (action) {
  try {
    const token = yield select(selectToken)
    const { id } = action.payload
    yield put(application.startRequest())
    yield call(api.private.deleteReview, id, token)
    yield call(fetchReviews)
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export const editReview = function * (action) {
  try {
    const token = yield select(selectToken)
    yield put(application.startRequest())
    yield call(api.private.editReview, action.payload, token)
    yield put(router.push('/admin/reviews'))
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export const fetchReviews = function * () {
  try {
    const token = yield select(selectToken)
    yield put(application.startRequest())
    const response = yield call(api.private.getReviews, token)
    yield put(A.setReviews(response))
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export const publishReview = function * (action) {
  try {
    const token = yield select(selectToken)
    yield put(application.startRequest())
    yield call(api.private.publishReview, action.payload, token)
    yield call(fetchReviews)
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export const unpublishReview = function * (action) {
  try {
    const token = yield select(selectToken)
    yield put(application.startRequest())
    yield call(api.private.unpublishReview, action.payload, token)
    yield call(fetchReviews)
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export default function * () {
  yield takeLatest(AT.ADMIN_CREATE_REVIEW, createReview)
  yield takeLatest(AT.ADMIN_DELETE_REVIEW, deleteReview)
  yield takeLatest(AT.ADMIN_EDIT_REVIEW, editReview)
  yield takeLatest(AT.ADMIN_FETCH_REVIEWS, fetchReviews)
  yield takeLatest(AT.ADMIN_PUBLISH_REVIEW, publishReview)
  yield takeLatest(AT.ADMIN_UNPUBLISH_REVIEW, unpublishReview)
}
