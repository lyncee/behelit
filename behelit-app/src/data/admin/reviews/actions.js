import * as AT from './actionTypes'

export const createReview = (name, company, role, message) => ({ type: AT.ADMIN_CREATE_REVIEW, payload: { name, company, role, message } })

export const deleteReview = (id) => ({ type: AT.ADMIN_DELETE_REVIEW, payload: { id } })

export const editReview = (id, name, company, role, message) => ({ type: AT.ADMIN_EDIT_REVIEW, payload: { id, name, company, role, message } })

export const fetchReviews = () => ({ type: AT.ADMIN_FETCH_REVIEWS })

export const setReviews = (reviews) => ({ type: AT.ADMIN_SET_REVIEWS, payload: { reviews } })

export const publishReview = (id) => ({ type: AT.ADMIN_PUBLISH_REVIEW, payload: { id } })

export const unpublishReview = (id) => ({ type: AT.ADMIN_UNPUBLISH_REVIEW, payload: { id } })
