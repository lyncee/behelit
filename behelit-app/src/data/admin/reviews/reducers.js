import * as AT from './actionTypes'

const INITIAL_STATE = []

const review = (state = INITIAL_STATE, action) => {
  const { type, payload } = action

  switch (type) {
    case AT.ADMIN_SET_REVIEWS: {
      return payload.reviews
    }
    default: {
      return state
    }
  }
}

export default review
