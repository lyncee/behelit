import { compose, filter, head, path } from 'ramda'

export const selectReview = id => compose(head, filter(x => x._id === id), selectReviews)

export const selectReviews = path(['admin', 'reviews'])
