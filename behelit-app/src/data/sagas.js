import * as admin from './admin/sagas'
import * as alerts from './alerts/sagas'
import * as application from './application/sagas'
import * as articles from './articles/sagas'
import * as auth from './auth/sagas'
import * as comments from './comments/sagas'
import * as projects from './projects/sagas'
import * as reviews from './reviews/sagas'

export {
  admin,
  alerts,
  application,
  articles,
  auth,
  comments,
  projects,
  reviews
}
