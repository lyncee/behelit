import { all, fork } from 'redux-saga/effects'
import admin from './admin/sagas.js'
import alerts from './alerts/sagas.js'
import application from './application/sagas.js'
import articles from './articles/sagas.js'
import auth from './auth/sagas.js'
import comments from './comments/sagas.js'
import projects from './projects/sagas.js'
import reviews from './reviews/sagas.js'

const sagas = function * () {
  yield all([
    fork(admin),
    fork(alerts),
    fork(application),
    fork(articles),
    fork(auth),
    fork(comments),
    fork(projects),
    fork(reviews)
  ])
}

export default sagas
