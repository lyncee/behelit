import * as admin from './admin/actionTypes'
import * as alerts from './alerts/actionTypes'
import * as application from './application/actionTypes'
import * as articles from './articles/actionTypes'
import * as auth from './auth/actionTypes'
import * as comments from './comments/actionTypes'
import * as projects from './projects/actionTypes'
import * as reviews from './reviews/actionTypes'

export {
  admin,
  alerts,
  application,
  articles,
  auth,
  comments,
  projects,
  reviews
}
