import { append, contains } from 'ramda'
import * as AT from './actionTypes'

const INITIAL_STATE = []

const articles = (state = INITIAL_STATE, action) => {
  const { type, payload } = action

  switch (type) {
    case AT.SET_ARTICLE: {
      const { article } = payload
      return !contains(article, state) ? append(article, state) : state
    }

    case AT.SET_ARTICLES: {
      const { articles } = payload
      return articles
    }

    default: {
      return state
    }
  }
}

export default articles
