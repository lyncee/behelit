import * as AT from './actionTypes'

export const fetchArticle = (id) => ({ type: AT.FETCH_ARTICLE, payload: { id } })

export const fetchArticles = () => ({ type: AT.FETCH_ARTICLES })

export const setArticle = (article) => ({ type: AT.SET_ARTICLE, payload: { article } })

export const setArticles = (articles) => ({ type: AT.SET_ARTICLES, payload: { articles } })
