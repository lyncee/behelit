import { compose, filter, head, path } from 'ramda'

export const selectArticle = id => compose(head, filter(x => x._id === id), selectArticles)

export const selectArticles = path(['articles'])
