import { takeLatest, put, call } from 'redux-saga/effects'
import { application, alerts } from '../actions'
import * as A from './actions'
import * as AT from './actionTypes'
import api from 'services/api'

export const fetchArticle = function * (action) {
  try {
    const { id } = action.payload
    yield put(application.startRequest())
    const response = yield call(api.public.getArticle, id)
    yield put(A.setArticle(response))
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export const fetchArticles = function * () {
  try {
    yield put(application.startRequest())
    const response = yield call(api.public.getArticles)
    yield put(A.setArticles(response))
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export default function * () {
  yield takeLatest(AT.FETCH_ARTICLE, fetchArticle)
  yield takeLatest(AT.FETCH_ARTICLES, fetchArticles)
}
