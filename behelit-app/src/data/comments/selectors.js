import { path } from 'ramda'

export const selectComments = articleId => path(['comments', articleId])
