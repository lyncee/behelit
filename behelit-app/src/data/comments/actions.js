import * as AT from './actionTypes'

export const createComment = (name, email, message, articleId) => ({ type: AT.CREATE_COMMENT, payload: { name, email, message, articleId } })

export const fetchComments = (articleId) => ({ type: AT.FETCH_COMMENTS, payload: { articleId } })

export const setComments = (comments, articleId) => ({ type: AT.SET_COMMENTS, payload: { comments, articleId } })
