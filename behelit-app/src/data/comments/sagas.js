import { takeLatest, put, call } from 'redux-saga/effects'
import { application, alerts } from '../actions'
import * as A from './actions'
import * as AT from './actionTypes'
import api from 'services/api'

export const createComment = function * (action) {
  try {
    const comment = action.payload
    const { articleId } = comment
    yield put(application.startRequest())
    yield call(api.public.createComment, comment)
    yield put(alerts.displaySuccess('Your comment has been published.'))
    yield call(fetchComments, { payload: { articleId } })
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export const fetchComments = function * (action) {
  try {
    const { articleId } = action.payload
    yield put(application.startRequest())
    const response = yield call(api.public.getComments, articleId)
    yield put(A.setComments(response, articleId))
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

export default function * () {
  yield takeLatest(AT.CREATE_COMMENT, createComment)
  yield takeLatest(AT.FETCH_COMMENTS, fetchComments)
}
