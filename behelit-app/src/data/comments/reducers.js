import { assoc } from 'ramda'
import * as AT from './actionTypes'

const INITIAL_STATE = {}

const comments = (state = INITIAL_STATE, action) => {
  const { type, payload } = action

  switch (type) {
    case AT.SET_COMMENTS: {
      const { comments, articleId } = payload
      return assoc(articleId, comments, state)
    }

    default: {
      return state
    }
  }
}

export default comments
