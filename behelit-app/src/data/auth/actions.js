import * as AT from './actionTypes'

export const login = (username, password) => ({ type: AT.LOGIN, payload: { username, password } })

export const authenticate = (username, token) => ({ type: AT.AUTHENTICATE, payload: { username, token } })

export const logout = () => ({ type: AT.LOGOUT })
