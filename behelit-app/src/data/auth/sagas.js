import { takeLatest, put, call } from 'redux-saga/effects'
import { application, alerts, form, router } from '../actions'
import * as A from './actions'
import * as AT from './actionTypes'
import api from 'services/api'

export const login = function * (action) {
  try {
    const { username, password } = action.payload
    yield put(application.startRequest())
    const response = yield call(api.public.login, username, password)
    yield put(A.authenticate(username, response.token))
    yield put(router.push('/admin'))
    yield put(form.reset('login'))
  } catch (e) {
    yield put(alerts.displayError(e.message))
  } finally {
    yield put(application.stopRequest())
  }
}

const sagas = function * () {
  yield takeLatest(AT.LOGIN, login)
}

export default sagas
