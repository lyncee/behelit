import { path } from 'ramda'

export const selectUsername = path(['auth', 'username'])

export const selectToken = path(['auth', 'token'])
