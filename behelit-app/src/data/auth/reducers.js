import * as AT from './actionTypes'

const INITIAL_STATE = {}

const auth = (state = INITIAL_STATE, action) => {
  const { type, payload } = action

  switch (type) {
    case AT.AUTHENTICATE: {
      const { username, token } = payload
      return Object.assign({}, INITIAL_STATE, { username, token })
    }
    case AT.LOGOUT: {
      return INITIAL_STATE
    }
    default: {
      return state
    }
  }
}

export default auth
