import React, { Component } from 'react'
import { Redirect, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'

import AdminLayout from 'layouts/Admin'
import PublicLayout from 'layouts/Public'
import About from './About'
import Blog from './Blog'
import BlogPost from './BlogPost'
import Contact from './Contact'
import Login from './Login'
import Projects from './Projects'
import Reviews from './Reviews'
import ArticleList from './Admin/Articles/List'
import ArticleCreate from './Admin/Articles/Create'
import ArticleEdit from './Admin/Articles/Edit'
import CommentList from './Admin/Comments/List'
import ProjectList from './Admin/Projects/List'
import ProjectCreate from './Admin/Projects/Create'
import ProjectEdit from './Admin/Projects/Edit'
import ReviewList from './Admin/Reviews/List'
import ReviewCreate from './Admin/Reviews/Create'
import ReviewEdit from './Admin/Reviews/Edit'
import TagList from './Admin/Tags/List'
import TagCreate from './Admin/Tags/Create'
import TagEdit from './Admin/Tags/Edit'
import UserList from './Admin/Users/List'
import UserCreate from './Admin/Users/Create'
import UserEdit from './Admin/Users/Edit'

class App extends React.Component {
  render () {
    const { store, history } = this.props

    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <Switch>
            <Redirect from='/' to='/blog' exact />
            <PublicLayout path='/about' component={About} />
            <PublicLayout path='/blog/:blogPostId' component={BlogPost} />
            <PublicLayout path='/blog' component={Blog} />
            <PublicLayout path='/contact' component={Contact} />
            <PublicLayout path='/login' component={Login} />
            <PublicLayout path='/projects' component={Projects} />
            <PublicLayout path='/reviews' component={Reviews} />
            <Redirect from='/admin' to='/admin/articles' exact />
            <AdminLayout path='/admin/articles/edit/:articleId' component={ArticleEdit} />
            <AdminLayout path='/admin/articles/create' component={ArticleCreate} />
            <AdminLayout path='/admin/articles' component={ArticleList} />
            <AdminLayout path='/admin/comments' component={CommentList} />
            <AdminLayout path='/admin/projects/edit/:projectId' component={ProjectEdit} />
            <AdminLayout path='/admin/projects/create' component={ProjectCreate} />
            <AdminLayout path='/admin/projects' component={ProjectList} />
            <AdminLayout path='/admin/reviews/edit/:reviewId' component={ReviewEdit} />
            <AdminLayout path='/admin/reviews/create' component={ReviewCreate} />
            <AdminLayout path='/admin/reviews' component={ReviewList} />
            <AdminLayout path='/admin/tags/edit/:tagId' component={TagEdit} />
            <AdminLayout path='/admin/tags/create' component={TagCreate} />
            <AdminLayout path='/admin/tags' component={TagList} />
            <AdminLayout path='/admin/users/edit/:userId' component={UserEdit} />
            <AdminLayout path='/admin/users/create' component={UserCreate} />
            <AdminLayout path='/admin/users' component={UserList} />
          </Switch>
        </ConnectedRouter>
      </Provider>
    )
  }
}

export default App
