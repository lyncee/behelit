import React from 'react'
import styled from 'styled-components'
import { reduxForm, Field } from 'redux-form'

import { isRequired } from 'services/form'
import { Button, Panel, Text } from 'behelit-components'
import { Form, TextBox, MarkdownBox } from 'components'

const Wrapper = styled.div`
  width: 100%;
`

const CommentForm = props => {
  const { handleSubmit, handleToggle, toggled, pristine, dirty, invalid } = props
  return toggled ? (
    <Wrapper>
      <Panel>
        <Form onSubmit={handleSubmit}>
          <Button nature='secondary' fullwidth onClick={handleToggle}>Cancel</Button>
          <Text size='16px' weight={500}>Name:</Text>
          <Field name='name' component={TextBox} validate={[isRequired]} />
          <Text size='16px' weight={500}>Email:</Text>
          <Field name='email' component={TextBox} />
          <Text size='16px' weight={500}>Comment:</Text>
          <Field name='message' component={MarkdownBox} validate={[isRequired]} previewModeEnabled />
          <Button type='submit' fullwidth disabled={pristine || (dirty && invalid)}>Send</Button>
        </Form>
      </Panel>
    </Wrapper>
  ) : (
    <Wrapper>
      <Panel>
        <Button fullwidth onClick={handleToggle}>Leave a comment</Button>
      </Panel>
    </Wrapper>
  )
}

export default reduxForm({ form: 'comment' })(CommentForm)
