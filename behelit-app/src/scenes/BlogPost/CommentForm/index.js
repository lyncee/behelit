import React from 'react'
import { connect } from 'react-redux'
import { compose, bindActionCreators } from 'redux'
import { formValueSelector } from 'redux-form'
import ui from 'redux-ui'

import { actions } from 'data'
import Reviews from './template'

class CommentFormContainer extends React.Component {
  constructor (props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleToggle = this.handleToggle.bind(this)
  }

  handleSubmit (e) {
    e.preventDefault()
    const { name, email, message, articleId } = this.props
    this.props.commentActions.createComment(name, email, message, articleId)
    this.handleToggle()
  }

  handleToggle () {
    this.props.updateUI({ toggled: !this.props.ui.toggled })
    this.props.formActions.destroy('comment')
  }

  render () {
    return <Reviews handleSubmit={this.handleSubmit} handleToggle={this.handleToggle} toggled={this.props.ui.toggled} {...this.props} />
  }
}

const mapStateToProps = (state, ownProps) => ({
  name: formValueSelector('comment')(state, 'name'),
  email: formValueSelector('comment')(state, 'email'),
  message: formValueSelector('comment')(state, 'message')
})

const mapDispatchToProps = (dispatch) => ({
  commentActions: bindActionCreators(actions.comments, dispatch),
  formActions: bindActionCreators(actions.form, dispatch)
})

const enhance = compose(
  ui({ state: { toggled: false } }),
  connect(mapStateToProps, mapDispatchToProps)
)

export default enhance(CommentFormContainer)
