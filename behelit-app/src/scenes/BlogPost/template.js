import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { isNil } from 'ramda'

import { SplashScreen } from 'components'
import Article from './Article'
import CommentForm from './CommentForm'
import CommentList from './CommentList'

const Wrapper = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;

  @media(min-width: 992px) {
    flex-direction: row;
  }
`
const ColumnLeft = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;

  @media(min-width: 992px) {
    width: 70%;
  }
`
const ColumnRight = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;
  margin-top: 10px;

  & > :first-child { margin-bottom: 10px; }

  @media(min-width: 992px) {
    margin-left: 10px;
    margin-top: 0;
    width: 30%;
  }
`

const BlogPost = props => {
  const { article, articleId } = props

  return !isNil(article) ? (
    <Wrapper>
      <ColumnLeft>
        <Article article={article} />
      </ColumnLeft>
      <ColumnRight>
        <CommentForm articleId={articleId} />
        <CommentList articleId={articleId} />
      </ColumnRight>
    </Wrapper>
  ) : (
    <Wrapper>
      <SplashScreen />
    </Wrapper>
  )
}

BlogPost.propTypes = {
  article: PropTypes.object,
  articleId: PropTypes.string,
  comments: PropTypes.array
}

export default BlogPost
