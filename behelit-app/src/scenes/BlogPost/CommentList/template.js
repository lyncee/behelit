import React from 'react'
import PropTypes from 'prop-types'
import styled, { injectGlobal } from 'styled-components'

import CommentItem from './CommentItem'

injectGlobal`
  div *::-webkit-scrollbar { display: none; }
`

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;

  & > :last-child { margin-bottom: 0; }

  @media(min-width: 992px) {
    height: 1000px;
    overflow: auto;
  }
`

const CommentList = props => (
  <Wrapper>
    {props.comments.map((comment, index) => <CommentItem key={index} comment={comment} />)}
  </Wrapper>
)

CommentList.propTypes = {
  comments: PropTypes.array
}

CommentList.defaultProps = {
  comments: []
}

export default CommentList
