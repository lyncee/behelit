import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { actions, selectors } from 'data'
import CommentList from './template'

class CommentListContainer extends React.Component {
  render () {
    return <CommentList comments={this.props.comments} />
  }
}

const mapStateToProps = (state, ownProps) => ({
  comments: selectors.comments.selectComments(ownProps.articleId)(state)
})

const mapDispatchToProps = (dispatch) => ({
  commentActions: bindActionCreators(actions.comments, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(CommentListContainer)
