import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { Color, Markdown, Text } from 'behelit-components'

const Wrapper = styled.div`
  position: relative;
  width: 100%;
  margin-bottom: 10px;
  padding: 10px;
  box-sizing: border-box;
  background-color: ${Color('gray-1')};
  border: 1px solid ${Color('gray-2')};
  border-radius: 5px;
`
const Content = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100% ;
`
const Footer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 100% ;
`

const CommentItem = props => {
  const { comment } = props

  return (
    <Wrapper>
      <Content>
        <Markdown content={comment.message} />
      </Content>
      <Footer>
        <Text size='14px' weight={500}>{comment.name}</Text>
        <Text size='14px' weight={300} italic>{comment.createdAt}</Text>
      </Footer>
    </Wrapper>
  )
}

CommentItem.propTypes = {
  comment: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    message: PropTypes.string.isRequired,
    createdAt: PropTypes.string.isRequired
  })
}

export default CommentItem
