import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { actions, selectors } from 'data'
import BlogPost from './template.js'

class BlogPostContainer extends React.Component {
  componentWillMount () {
    const { articleId } = this.props
    this.props.articleActions.fetchArticle(articleId)
    this.props.commentActions.fetchComments(articleId)
  }

  render () {
    const { article, articleId } = this.props
    return <BlogPost article={article} articleId={articleId} />
  }
}

const mapStateToProps = (state, ownProps) => ({
  article: selectors.articles.selectArticle(ownProps.match.params.blogPostId)(state),
  articleId: ownProps.match.params.blogPostId
})

const mapDispatchToProps = (dispatch) => ({
  articleActions: bindActionCreators(actions.articles, dispatch),
  commentActions: bindActionCreators(actions.comments, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(BlogPostContainer)
