import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { Color, Devicon, Text } from 'behelit-components'

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;
  box-sizing: border-box;
  background-color: ${Color('gray-1')};
  border: 1px solid ${Color('gray-2')};
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
  overflow: hidden;
`
const Category = styled.div`
  display: none;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 150px;
  height: 150px;
  background-color: ${Color('gray-2')};

  @media(min-width: 768px) {
    display: flex;
  }
`
const Description = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: space-between;
  width: 100%;
  padding: 10px;
  box-sizing: border-box;

  @media(min-width: 768px) {
    width: calc(100% - 150px);
  }
`
const Title = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;

  & > span { margin-bottom: 5px; }
`
const Tags = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;
  flex-wrap: wrap;
  width: 100%;

  & > * { margin-right: 5px; }
`
const Footer = props => {
  const { article } = props

  return (
    <Wrapper>
      <Category>
        <Devicon name={article.category} />
      </Category>
      <Description>
        <Title>
          <Text size='24px' weight={500} color='brand-primary' uppercase>{article.title}</Text>
          <Text size='16px' weight={500} color='gray-6'>{article.description}</Text>
          <Text size='14px' weight={300} color='gray-6' italic>{`Published: ${article.createdAt}`}</Text>
        </Title>
        <Tags>
          {article.tags.map((tag, index) => <Text key={index} size='14px' weight={500} color={index % 2 === 0 ? 'brand-primary' : 'gray-6'}>{tag}</Text>)}
        </Tags>
      </Description>
    </Wrapper>
  )
}

Footer.propTypes = {
  article: PropTypes.object
}

export default Footer
