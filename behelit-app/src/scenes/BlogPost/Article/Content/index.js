import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { Color, Markdown } from 'behelit-components'

const Wrapper = styled.div`
  width: 100%;
  padding: 15px;
  box-sizing: border-box;
  background-color: ${Color('gray-1')};
  border-left: 1px solid ${Color('gray-2')};
  border-right: 1px solid ${Color('gray-2')};
`

const Content = props => {
  const { content } = props.article

  return (
    <Wrapper>
      <Markdown content={content} />
    </Wrapper>
  )
}

Content.propTypes = {
  article: PropTypes.shape({
    content: PropTypes.string
  }).isRequired
}

Content.defaultProps = {
  article: {
    content: ''
  }
}

export default Content
