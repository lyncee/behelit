import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import Header from './Header'
import Content from './Content'
import Footer from './Footer'

const Wrapper = styled.section`
  width: 100%;
`

const Article = props => (
  <Wrapper>
    <Header {...props} />
    <Content {...props} />
    <Footer {...props} />
  </Wrapper>
)

Article.propTypes = {
  article: PropTypes.object
}

export default Article
