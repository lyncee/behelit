import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import avatar from './avatar.png'
import { Color, Text } from 'behelit-components'

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;
  padding: 10px;
  box-sizing: border-box;
  background-color: ${Color('gray-2')};
  border: 1px solid ${Color('gray-2')};
  border-bottom-left-radius: 5px;
  border-bottom-right-radius: 5px;
`
const Avatar = styled.img.attrs({ src: avatar })`
  width: 100px;
  height: 100px;
`
const Description = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: calc(100% - 100px);
  padding: 15px;
  box-sizing: border-box;
`

const Footer = props => {
  const { article } = props

  return (
    <Wrapper>
      <Avatar />
      <Description>
        <Text size='20px' weight={300} color='brand-primary' capitalize>{article.author}</Text>
        <Text italic>
          I'm a full stack developer with over 9 years of experience working in .NET, HTML5, CSS3, Javascript (React & Node.js).
          I love programming, playing videogames and learning new things !
        </Text>
      </Description>
    </Wrapper>
  )
}

Footer.propTypes = {
  article: PropTypes.object
}

export default Footer
