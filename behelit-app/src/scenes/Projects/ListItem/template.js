import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { Color, Icon, Markdown, Text } from 'behelit-components'

const Wrapper = styled.div`
  position: relative;
  width: 100%;
  padding: 10px;
  margin-bottom: 10px;
  box-sizing: border-box;
  background-color: ${Color('gray-1')};
  border: 1px solid ${Color('gray-2')};

  & > :last-child { padding-bottom: 15px; }
`
const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: flex-start;
  width: 100%;
  padding-top: 15px;
  box-sizing: border-box;

  @media(min-width: 992px) { 
    flex-direction: row;
    justify-content: flex-start;
  }
`
const Description = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;
  padding: 15px;
  box-sizing: border-box;

  &  > * { margin-bottom: 10px; }

  @media(min-width: 992px) { width: 60%; }
`
const Details = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;
  padding: 15px;
  box-sizing: border-box;
`
const Tags = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;
  flex-wrap: wrap;
  width: 100%;
  padding: 15px;
  box-sizing: border-box;

  & > * { padding: 5px; }
  
  @media(min-width: 992px) { width: 40%; }
`
const MoreToggler = styled.div`
  position: absolute;
  bottom: -5px;
  right: -5px;
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  width: 80px;
  height: 30px;
  padding: 5px;
  box-sizing: border-box;
  background-color: ${Color('gray-1')};
  border: 1px solid ${Color('gray-2')};
  border-radius: 5px;
  cursor: pointer;
  z-index: 1;
  transition: 0.3s;

  &:hover {
    & > * { color: ${Color('brand-primary')}; }
  }
`

const ListItem = props => {
  const { project, toggled, handleToggle } = props

  return (
    <Wrapper>
      <MoreToggler onClick={handleToggle} toggled={toggled}>
        <Icon size='16px' color={toggled ? 'brand-primary' : 'black'} name={`${toggled ? 'arrow-up' : 'arrow-down'}`} cursor />
        <Text size='16px' color={toggled ? 'brand-primary' : 'black'}>{toggled ? 'Less' : 'More'}</Text>
      </MoreToggler>
      <Container>
        <Description>
          <Text size='20px' weight={700} color='brand-primary' uppercase>{project.title}</Text>
          <Text weight={700} color='gray-6' capitalize>{`${project.company} - ${project.year}`}</Text>
          <Text>{project.summary}</Text>
        </Description>
        <Tags>
          { project.tags.map((tag, index) => <Text key={index} weight={500} color={index % 2 === 0 ? 'brand-primary' : 'gray-6'} capitalize>{tag}</Text>)}
        </Tags>
      </Container>
      { toggled &&
        <Container>
          <Details>
            <Markdown content={project.description} />
          </Details>
        </Container>
      }
    </Wrapper>
  )
}

ListItem.propTypes = {
  project: PropTypes.shape({
    title: PropTypes.string.isRequired,
    summary: PropTypes.string.isRequired,
    company: PropTypes.string.isRequired,
    year: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    tags: PropTypes.array.isRequired
  }).isRequired
}

export default ListItem
