import React from 'react'
import PropTypes from 'prop-types'
import ui from 'redux-ui'

import ListItem from './template.js'

class ListItemContainer extends React.Component {
  constructor (props) {
    super(props)
    this.handleToggle = this.handleToggle.bind(this)
  }

  handleToggle () {
    this.props.updateUI({ toggled: !this.props.ui.toggled })
  }

  render () {
    return <ListItem toggled={this.props.ui.toggled} handleToggle={this.handleToggle} {...this.props} />
  }
}

ListItemContainer.propTypes = {
  project: PropTypes.shape({
    title: PropTypes.string.isRequired,
    summary: PropTypes.string.isRequired,
    company: PropTypes.string.isRequired,
    year: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    tags: PropTypes.array.isRequired
  }).isRequired
}

export default ui({ state: { toggled: false } })(ListItemContainer)
