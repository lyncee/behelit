import React from 'react'
import styled from 'styled-components'
import { isEmpty } from 'ramda'

import { SplashScreen } from 'components'
import ListItem from './ListItem'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;
  height: 100%;
`

const Projects = props => {
  const { projects } = props

  return !isEmpty(projects) ? (
    <Wrapper>
      {projects.map((project, index) => <ListItem key={index} project={project} />)}
    </Wrapper>
  ) : (
    <Wrapper>
      <SplashScreen />
    </Wrapper>
  )
}

export default Projects
