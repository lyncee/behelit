import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { actions, selectors } from 'data'
import Projects from './template.js'

class ProjectsContainer extends React.Component {
  componentWillMount () {
    this.props.projectActions.fetchProjects()
  }

  render () {
    return <Projects {...this.props} />
  }
}

const mapStateToProps = (state) => ({
  projects: selectors.projects.selectProjects(state)
})

const mapDispatchToProps = (dispatch) => ({
  projectActions: bindActionCreators(actions.projects, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(ProjectsContainer)
