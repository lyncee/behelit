import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { formValueSelector } from 'redux-form'
import { isEmpty, map } from 'ramda'

import { actions, selectors } from 'data'
import Create from './template.js'

class CreateContainer extends React.Component {
  constructor (props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentWillMount () {
    if (isEmpty(this.props.tagItems)) { this.props.tagActions.fetchTags() }
  }

  handleSubmit (e) {
    e.preventDefault()
    const { title, summary, description, company, year, tags } = this.props
    this.props.projectActions.createProject(title, summary, description, company, year, tags)
  }

  render () {
    const tagList = map(x => ({ text: x.title, value: x._id }), this.props.tagItems)
    return <Create handleSubmit={this.handleSubmit} tagList={tagList} content={this.props.description} />
  }
}

const mapStateToProps = (state, ownProps) => ({
  title: formValueSelector('createProject')(state, 'title'),
  summary: formValueSelector('createProject')(state, 'summary'),
  description: formValueSelector('createProject')(state, 'description'),
  company: formValueSelector('createProject')(state, 'company'),
  year: formValueSelector('createProject')(state, 'year'),
  tags: formValueSelector('createProject')(state, 'tags'),
  tagItems: selectors.admin.tags.selectTags(state)
})

const mapDispatchToProps = (dispatch) => ({
  formActions: bindActionCreators(actions.form, dispatch),
  projectActions: bindActionCreators(actions.admin.projects, dispatch),
  tagActions: bindActionCreators(actions.admin.tags, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(CreateContainer)
