import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { actions, selectors } from 'data'
import List from './template.js'

class ListContainer extends React.Component {
  constructor (props) {
    super(props)
    this.handleCreate = this.handleCreate.bind(this)
    this.handleDelete = this.handleDelete.bind(this)
    this.handleEdit = this.handleEdit.bind(this)
    this.handlePublish = this.handlePublish.bind(this)
    this.handleUnpublish = this.handleUnpublish.bind(this)
  }

  componentWillMount () {
    this.props.projectActions.fetchProjects()
  }

  handleCreate () {
    this.props.routerActions.push('/admin/projects/create')
  }

  handleDelete (id) {
    this.props.projectActions.deleteProject(id)
  }

  handleEdit (id) {
    this.props.routerActions.push(`/admin/projects/edit/${id}`)
  }

  handlePublish (id) {
    this.props.projectActions.publishProject(id)
  }

  handleUnpublish (id) {
    this.props.projectActions.unpublishProject(id)
  }

  render () {
    return <List
      {...this.props}
      handleCreate={this.handleCreate}
      handleDelete={this.handleDelete}
      handleEdit={this.handleEdit}
      handlePublish={this.handlePublish}
      handleUnpublish={this.handleUnpublish}
     />
  }
}

const mapStateToProps = (state) => ({
  projects: selectors.admin.projects.selectProjects(state)
})

const mapDispatchToProps = (dispatch) => ({
  projectActions: bindActionCreators(actions.admin.projects, dispatch),
  routerActions: bindActionCreators(actions.router, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(ListContainer)
