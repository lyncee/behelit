import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { formValueSelector } from 'redux-form'
import { map, isEmpty } from 'ramda'

import { actions, selectors } from 'data'
import Edit from './template.js'

class EditContainer extends React.Component {
  constructor (props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentWillMount () {
    if (isEmpty(this.props.tagItems)) { this.props.tagActions.fetchTags() }
    this.props.formActions.initialize('editProject', {
      title: this.props.project.title,
      summary: this.props.project.summary,
      description: this.props.project.description,
      company: this.props.project.company,
      year: this.props.project.year,
      tags: this.props.project.tags
    })
  }

  handleSubmit (e) {
    e.preventDefault()
    const { id, title, summary, description, company, year, tags } = this.props
    this.props.projectActions.editProject(id, title, summary, description, company, year, tags)
  }

  render () {
    const tagList = map(x => ({ text: x.title, value: x._id }), this.props.tagItems)
    return <Edit title={this.props.title} handleSubmit={this.handleSubmit} tagList={tagList} content={this.props.description} />
  }
}

const mapStateToProps = (state, ownProps) => ({
  title: formValueSelector('editProject')(state, 'title'),
  summary: formValueSelector('editProject')(state, 'summary'),
  description: formValueSelector('editProject')(state, 'description'),
  company: formValueSelector('editProject')(state, 'company'),
  year: formValueSelector('editProject')(state, 'year'),
  tags: formValueSelector('editProject')(state, 'tags'),
  tagItems: selectors.admin.tags.selectTags(state),
  project: selectors.admin.projects.selectProject(ownProps.match.params.projectId)(state),
  id: ownProps.match.params.projectId
})

const mapDispatchToProps = (dispatch) => ({
  formActions: bindActionCreators(actions.form, dispatch),
  projectActions: bindActionCreators(actions.admin.projects, dispatch),
  tagActions: bindActionCreators(actions.admin.tags, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(EditContainer)
