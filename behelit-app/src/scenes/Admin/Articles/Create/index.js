import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { formValueSelector } from 'redux-form'
import { isEmpty, keysIn, map } from 'ramda'

import DeviconMap from 'behelit-components/src/Icons/Devicon/DeviconMap.js'
import { actions, selectors } from 'data'
import Create from './template.js'

class CreateContainer extends React.Component {
  constructor (props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentWillMount () {
    if (isEmpty(this.props.tagItems)) { this.props.tagActions.fetchTags() }
    if (isEmpty(this.props.userItems)) { this.props.userActions.fetchUsers() }
  }

  handleSubmit (e) {
    e.preventDefault()
    const { title, description, content, author, category, tags } = this.props
    this.props.articleActions.createArticle(title, description, content, author, category, tags)
  }

  render () {
    const { tagItems, userItems, content } = this.props
    const categoryList = map(x => ({ text: x, value: x }), keysIn(DeviconMap))
    const tagList = map(x => ({ text: x.title, value: x._id }), tagItems)
    const userList = map(x => ({ text: x.username, value: x._id }), userItems)
    return <Create handleSubmit={this.handleSubmit} categoryList={categoryList} tagList={tagList} userList={userList} content={content} />
  }
}

const mapStateToProps = (state, ownProps) => ({
  title: formValueSelector('createArticle')(state, 'title'),
  description: formValueSelector('createArticle')(state, 'description'),
  content: formValueSelector('createArticle')(state, 'content'),
  author: formValueSelector('createArticle')(state, 'author'),
  category: formValueSelector('createArticle')(state, 'category'),
  tags: formValueSelector('createArticle')(state, 'tags'),
  tagItems: selectors.admin.tags.selectTags(state),
  userItems: selectors.admin.users.selectUsers(state)
})

const mapDispatchToProps = (dispatch) => ({
  formActions: bindActionCreators(actions.form, dispatch),
  articleActions: bindActionCreators(actions.admin.articles, dispatch),
  tagActions: bindActionCreators(actions.admin.tags, dispatch),
  userActions: bindActionCreators(actions.admin.users, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(CreateContainer)
