import React from 'react'
import styled from 'styled-components'
import { reduxForm, Field } from 'redux-form'

import { isRequired } from 'services/form'
import { Button, Markdown, Text } from 'behelit-components'
import { Form, MarkdownBox, MultiSelectBox, SelectBox, TextBox, TextAreaBox } from 'components'

const Wrapper = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;

  & > :first-child { margin-right: 10px; }
`
const Header = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-start;  
  width: 100%;
  height: 50px;
`
const Content = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;  
  width: 100%;
`
const Container = styled.div`
  width: 50%;
  padding: 5px;
  box-sizing: border-box;
`

const Edit = props => {
  const { submitting, invalid, title, categoryList, tagList, userList, content, handleSubmit } = props

  return (
    <Wrapper>
      <Header>
        <Text size='20px' weight={500} color='brand-primary' uppercase>Edit [{title}]</Text>
      </Header>
      <Content>
        <Container>
          <Form onSubmit={handleSubmit}>
            <Text size='16px' weight={500}>Title:</Text>
            <Field name='title' component={TextBox} validate={[isRequired]} />
            <Text size='16px' weight={500}>Description:</Text>
            <Field name='description' component={TextAreaBox} validate={[isRequired]} rows={12} />
            <Text size='16px' weight={500}>Content:</Text>
            <Field name='content' component={MarkdownBox} validate={[isRequired]} />
            <Text size='16px' weight={500}>Author:</Text>
            <Field name='author' component={SelectBox} validate={[isRequired]} items={userList} />
            <Text size='16px' weight={500}>Category:</Text>
            <Field name='category' component={SelectBox} validate={[isRequired]} items={categoryList} searchEnabled />
            <Text size='16px' weight={500}>Tags:</Text>
            <Field name='tags' component={MultiSelectBox} items={tagList} />
            <Button fullwidth disabled={submitting || invalid}>Edit</Button>
          </Form>
        </Container>
        <Container>
          <Markdown content={content} />
        </Container>
      </Content>
    </Wrapper>
  )
}

export default reduxForm({ form: 'editArticle' })(Edit)
