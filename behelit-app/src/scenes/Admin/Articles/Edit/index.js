import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { formValueSelector } from 'redux-form'
import { isEmpty, keysIn, map } from 'ramda'

import DeviconMap from 'behelit-components/src/Icons/Devicon/DeviconMap.js'
import { actions, selectors } from 'data'
import Edit from './template.js'

class EditContainer extends React.Component {
  constructor (props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentWillMount () {
    if (isEmpty(this.props.tagItems)) { this.props.tagActions.fetchTags() }
    if (isEmpty(this.props.userItems)) { this.props.userActions.fetchUsers() }
    this.props.formActions.initialize('editArticle', {
      title: this.props.article.title,
      description: this.props.article.description,
      content: this.props.article.content,
      author: this.props.article.author,
      category: this.props.article.category,
      tags: this.props.article.tags
    })
  }

  handleSubmit (e) {
    e.preventDefault()
    const { id, title, description, content, author, category, tags } = this.props
    this.props.articleActions.editArticle(id, title, description, content, author, category, tags)
  }

  render () {
    const { tagItems, userItems, content } = this.props
    const categoryList = map(x => ({ text: x, value: x }), keysIn(DeviconMap))
    const tagList = map(x => ({ text: x.title, value: x._id }), tagItems)
    const userList = map(x => ({ text: x.username, value: x._id }), userItems)
    return <Edit title={this.props.title} handleSubmit={this.handleSubmit} categoryList={categoryList} tagList={tagList} userList={userList} content={content} />
  }
}

const mapStateToProps = (state, ownProps) => ({
  title: formValueSelector('editArticle')(state, 'title'),
  description: formValueSelector('editArticle')(state, 'description'),
  content: formValueSelector('editArticle')(state, 'content'),
  author: formValueSelector('editArticle')(state, 'author'),
  category: formValueSelector('editArticle')(state, 'category'),
  tags: formValueSelector('editArticle')(state, 'tags'),
  tagItems: selectors.admin.tags.selectTags(state),
  userItems: selectors.admin.users.selectUsers(state),
  article: selectors.admin.articles.selectArticle(ownProps.match.params.articleId)(state),
  id: ownProps.match.params.articleId
})

const mapDispatchToProps = (dispatch) => ({
  articleActions: bindActionCreators(actions.admin.articles, dispatch),
  formActions: bindActionCreators(actions.form, dispatch),
  tagActions: bindActionCreators(actions.admin.tags, dispatch),
  userActions: bindActionCreators(actions.admin.users, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(EditContainer)
