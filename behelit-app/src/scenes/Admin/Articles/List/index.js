import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { actions, selectors } from 'data'
import List from './template.js'

class ListContainer extends React.Component {
  constructor (props) {
    super(props)
    this.handleCreate = this.handleCreate.bind(this)
    this.handleDelete = this.handleDelete.bind(this)
    this.handleEdit = this.handleEdit.bind(this)
    this.handlePublish = this.handlePublish.bind(this)
    this.handleUnpublish = this.handleUnpublish.bind(this)
  }

  componentWillMount () {
    this.props.articleActions.fetchArticles()
  }

  handleCreate () {
    this.props.routerActions.push('/admin/articles/create')
  }

  handleDelete (id) {
    this.props.articleActions.deleteArticle(id)
  }

  handleEdit (id) {
    this.props.routerActions.push(`/admin/articles/edit/${id}`)
  }

  handlePublish (id) {
    this.props.articleActions.publishArticle(id)
  }

  handleUnpublish (id) {
    this.props.articleActions.unpublishArticle(id)
  }

  render () {
    return <List
      {...this.props}
      handleCreate={this.handleCreate}
      handleDelete={this.handleDelete}
      handleEdit={this.handleEdit}
      handlePublish={this.handlePublish}
      handleUnpublish={this.handleUnpublish}
    />
  }
}

const mapStateToProps = (state) => ({
  articles: selectors.admin.articles.selectArticles(state)
})

const mapDispatchToProps = (dispatch) => ({
  articleActions: bindActionCreators(actions.admin.articles, dispatch),
  routerActions: bindActionCreators(actions.router, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(ListContainer)
