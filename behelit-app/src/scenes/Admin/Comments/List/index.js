import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { actions, selectors } from 'data'
import List from './template.js'

class ListContainer extends React.Component {
  constructor (props) {
    super(props)
    this.handleDelete = this.handleDelete.bind(this)
    this.handlePublish = this.handlePublish.bind(this)
    this.handleUnpublish = this.handleUnpublish.bind(this)
  }

  componentWillMount () {
    this.props.commentActions.fetchComments()
  }

  handleDelete (id) {
    this.props.commentActions.deleteComment(id)
  }

  handlePublish (id) {
    this.props.commentActions.publishComment(id)
  }

  handleUnpublish (id) {
    this.props.commentActions.unpublishComment(id)
  }

  render () {
    return <List
      {...this.props}
      handleDelete={this.handleDelete}
      handlePublish={this.handlePublish}
      handleUnpublish={this.handleUnpublish}
    />
  }
}

const mapStateToProps = (state) => ({
  comments: selectors.admin.comments.selectComments(state)
})

const mapDispatchToProps = (dispatch) => ({
  commentActions: bindActionCreators(actions.admin.comments, dispatch),
  routerActions: bindActionCreators(actions.router, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(ListContainer)
