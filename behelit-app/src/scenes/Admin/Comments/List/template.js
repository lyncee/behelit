import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import moment from 'moment'

import { Button, Color, Icon, Text } from 'behelit-components'

const Wrapper = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;
`
const Table = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;
`
const Header = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  width: 100%;
  height: auto;
  min-height: 50px;
  font-size: 16px;
  font-weight: 700;
  text-transform: uppercase;
  color: ${Color('gray-5')};
  border-bottom: 1px solid ${Color('gray-3')};
  padding: 5px;
  box-sizing: border-box;
`
const Row = Header.extend`
  font-weight: 300;
  text-transform: none;

  &:hover { background-color: ${Color('gray-1')}; }
`
const CellName = styled.div`
  width: 200px;
`
const CellSummary = styled.div`
  width: calc(100% - 650px);
`
const CellIpAddress = styled.div`
  width: 150px;
`
const CellTime = styled.div`
  width: 150px;
`
const CellOptions = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  width: 150px;
`

const List = props => {
  const { comments, handleDelete, handlePublish, handleUnpublish } = props

  return (
    <Wrapper>
      <Table>
        <Header>
          <CellName>Name</CellName>
          <CellSummary>Message</CellSummary>
          <CellIpAddress>IP</CellIpAddress>
          <CellTime>Created at</CellTime>
          <CellOptions>#</CellOptions>
        </Header>
        { comments.map((comment, index) =>
          <Row key={index}>
            <CellName>{comment.name}</CellName>
            <CellSummary>{comment.message}</CellSummary>
            <CellIpAddress>{comment.ipAddress}</CellIpAddress>
            <CellTime>{moment(comment.createdAt).format('DD/MM/YYYY HH:mm:ss')}</CellTime>
            <CellOptions>
              <Icon name='thumbs-up' size='24px' color={comment.isPublished ? 'green' : 'gray-5'} cursor onClick={() => handlePublish(comment._id)} />
              <Icon name='thumbs-down' size='24px' color={!comment.isPublished ? 'red' : 'gray-5'} cursor onClick={() => handleUnpublish(comment._id)} />
              <Text>|</Text>
              <Icon name='trash' size='24px' color='gray-5' cursor onClick={() => handleDelete(comment._id)} />
            </CellOptions>
          </Row>
        )}
      </Table>
    </Wrapper>
  )
}

List.propTypes = {
  comments: PropTypes.array.isRequired
}

export default List
