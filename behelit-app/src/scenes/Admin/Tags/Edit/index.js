import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { formValueSelector } from 'redux-form'

import { actions, selectors } from 'data'
import Edit from './template.js'

class EditContainer extends React.Component {
  constructor (props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentWillMount () {
    this.props.formActions.initialize('editTag', {
      title: this.props.tag.title
    })
  }

  handleSubmit (e) {
    e.preventDefault()
    const { id, title } = this.props
    this.props.tagActions.editTag(id, title)
  }

  render () {
    return <Edit title={this.props.title} onSubmit={this.handleSubmit} />
  }
}

const mapStateToProps = (state, ownProps) => ({
  title: formValueSelector('editTag')(state, 'title'),
  tag: selectors.admin.tags.selectTag(ownProps.match.params.tagId)(state),
  id: ownProps.match.params.tagId
})

const mapDispatchToProps = (dispatch) => ({
  formActions: bindActionCreators(actions.form, dispatch),
  tagActions: bindActionCreators(actions.admin.tags, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(EditContainer)
