import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import moment from 'moment'

import { Button, Color, Icon } from 'behelit-components'

const Wrapper = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;
  height: 100%;
`
const Menu = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  width: 100%;
`
const Table = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;
  height: 100%;
  box-sizing: border-box;
`
const Header = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  width: 100%;
  height: auto;
  min-height: 50px;
  font-size: 16px;
  font-weight: 700;
  text-transform: uppercase;
  color: ${Color('gray-5')};
  border-bottom: 1px solid ${Color('gray-3')};
  padding: 5px;
  box-sizing: border-box;
`
const Rows = styled.div`
  width: 100%;
  height: 100%;
  overflow: auto;
`
const Row = Header.extend`
  font-weight: 300;
  text-transform: none;

  &:hover { background-color: ${Color('gray-1')}; }
`
const CellTitle = styled.div`
  width: calc(100% - 300px);
`
const CellTime = styled.div`
  width: 200px;
`
const CellOptions = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  width: 100px;
`

const List = props => {
  const { tags, handleCreate, handleDelete, handleEdit } = props

  return (
    <Wrapper>
      <Menu>
        <Button onClick={() => handleCreate()}>Create Tag</Button>
      </Menu>
      <Table>
        <Header>
          <CellTitle>Title</CellTitle>
          <CellTime>Created at</CellTime>
          <CellOptions>#</CellOptions>
        </Header>
        <Rows>
          { tags.map((tag, index) =>
            <Row key={index}>
              <CellTitle>{tag.title}</CellTitle>
              <CellTime>{moment(tag.createdAt).format('DD/MM/YYYY HH:mm:ss')}</CellTime>
              <CellOptions>
                <Icon name='pencil' size='24px' color='gray-5' cursor onClick={() => handleEdit(tag._id)} />
                <Icon name='trash' size='24px' color='gray-5' cursor onClick={() => handleDelete(tag._id)} />
              </CellOptions>
            </Row>
          )}
        </Rows>
      </Table>
    </Wrapper>
  )
}

List.propTypes = {
  tags: PropTypes.array.isRequired
}

export default List
