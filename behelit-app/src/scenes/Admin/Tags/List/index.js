import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { sortBy } from 'ramda'

import { actions, selectors } from 'data'
import List from './template.js'

class ListContainer extends React.Component {
  constructor (props) {
    super(props)
    this.handleCreate = this.handleCreate.bind(this)
    this.handleDelete = this.handleDelete.bind(this)
    this.handleEdit = this.handleEdit.bind(this)
  }

  componentWillMount () {
    this.props.tagActions.fetchTags()
  }

  handleCreate () {
    this.props.routerActions.push('/admin/tags/create')
  }

  handleDelete (id) {
    this.props.tagActions.deleteTag(id)
  }

  handleEdit (id) {
    this.props.routerActions.push(`/admin/tags/edit/${id}`)
  }

  render () {
    const tags = sortBy(x => x.title, this.props.tags)
    return <List tags={tags} handleCreate={this.handleCreate} handleDelete={this.handleDelete} handleEdit={this.handleEdit} />
  }
}

const mapStateToProps = (state) => ({
  tags: selectors.admin.tags.selectTags(state)
})

const mapDispatchToProps = (dispatch) => ({
  routerActions: bindActionCreators(actions.router, dispatch),
  tagActions: bindActionCreators(actions.admin.tags, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(ListContainer)
