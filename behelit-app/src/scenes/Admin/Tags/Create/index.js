import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { formValueSelector } from 'redux-form'

import { actions } from 'data'
import Create from './template.js'

class CreateContainer extends React.Component {
  constructor (props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit (e) {
    e.preventDefault()
    const { title } = this.props
    this.props.tagActions.createTag(title)
  }

  render () {
    return <Create handleSubmit={this.handleSubmit} />
  }
}

const mapStateToProps = (state, ownProps) => ({
  title: formValueSelector('createTag')(state, 'title')
})

const mapDispatchToProps = (dispatch) => ({
  formActions: bindActionCreators(actions.form, dispatch),
  tagActions: bindActionCreators(actions.admin.tags, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(CreateContainer)
