import React from 'react'
import styled from 'styled-components'
import { reduxForm, Field } from 'redux-form'

import { isRequired } from 'services/form'
import { Button, Panel, Text } from 'behelit-components'
import { Form, TextBox } from 'components'

const Wrapper = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;

  & > :first-child { margin-right: 10px; }
`
const Header = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-start;  
  width: 100%;
  height: 50px;
`
const Container = styled.div`
  width: 100%;  
  @media(min-width: 992px) { width: 50%; }
`

const Create = props => {
  const { handleSubmit, submitting, invalid } = props

  return (
    <Wrapper>
      <Header>
        <Text size='20px' weight={500} color='brand-primary' uppercase>Create Tag</Text>
      </Header>
      <Container>
        <Panel>
          <Form onSubmit={handleSubmit}>
            <Text size='16px' weight={500}>Title:</Text>
            <Field name='title' component={TextBox} validate={[isRequired]} />
            <Button fullwidth disabled={submitting || invalid}>Create</Button>
          </Form>
        </Panel>
      </Container>
    </Wrapper>
  )
}

export default reduxForm({ form: 'createTag' })(Create)
