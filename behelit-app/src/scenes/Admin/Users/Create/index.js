import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { formValueSelector } from 'redux-form'

import { actions } from 'data'
import Create from './template.js'

class CreateContainer extends React.Component {
  constructor (props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit (e) {
    e.preventDefault()
    const { username, email, password } = this.props
    this.props.userActions.createUser(username, email, password)
  }

  render () {
    return <Create handleSubmit={this.handleSubmit} />
  }
}

const mapStateToProps = (state, ownProps) => ({
  username: formValueSelector('createUser')(state, 'username'),
  email: formValueSelector('createUser')(state, 'email'),
  password: formValueSelector('createUser')(state, 'password')
})

const mapDispatchToProps = (dispatch) => ({
  formActions: bindActionCreators(actions.form, dispatch),
  userActions: bindActionCreators(actions.admin.users, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(CreateContainer)
