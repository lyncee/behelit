import React from 'react'
import styled from 'styled-components'
import { reduxForm, Field } from 'redux-form'

import { isRequired, isRequiredEmail } from 'services/form'
import { Button, Panel, Text } from 'behelit-components'
import { Form, TextBox } from 'components'

const Wrapper = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;

  & > :first-child { margin-right: 10px; }
`
const Header = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-start;  
  width: 100%;
  height: 50px;
`
const Container = styled.div`
  width: 50%;
`

const Edit = props => {
  const { onSubmit, submitting, invalid, title } = props

  return (
    <Wrapper>
      <Header>
        <Text size='20px' weight={500} color='brand-primary' uppercase>Edit [{title}]</Text>
      </Header>
      <Container>
        <Panel>
          <Form onSubmit={onSubmit}>
            <Text size='16px' weight={500}>Username:</Text>
            <Field name='username' component={TextBox} validate={[isRequired]} />
            <Text size='16px' weight={500}>Email:</Text>
            <Field name='email' component={TextBox} validate={[isRequiredEmail]} />
            <Button fullwidth disabled={submitting || invalid}>Edit</Button>
          </Form>
        </Panel>
      </Container>
    </Wrapper>
  )
}

export default reduxForm({ form: 'editUser' })(Edit)
