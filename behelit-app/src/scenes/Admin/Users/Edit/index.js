import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { formValueSelector } from 'redux-form'

import { actions, selectors } from 'data'
import Edit from './template.js'

class EditContainer extends React.Component {
  constructor (props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentWillMount () {
    this.props.formActions.initialize('editUser', {
      username: this.props.user.username,
      email: this.props.user.email
    })
  }

  handleSubmit (e) {
    e.preventDefault()
    const { id, username, email } = this.props
    this.props.userActions.editUser(id, username, email)
  }

  render () {
    return <Edit title={this.props.username} onSubmit={this.handleSubmit} />
  }
}

const mapStateToProps = (state, ownProps) => ({
  username: formValueSelector('editUser')(state, 'username'),
  email: formValueSelector('editUser')(state, 'email'),
  user: selectors.admin.users.selectUser(ownProps.match.params.userId)(state),
  id: ownProps.match.params.userId
})

const mapDispatchToProps = (dispatch) => ({
  formActions: bindActionCreators(actions.form, dispatch),
  userActions: bindActionCreators(actions.admin.users, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(EditContainer)
