import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import moment from 'moment'

import { Button, Color, Icon } from 'behelit-components'

const Wrapper = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;
`
const Table = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;
`
const Header = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  width: 100%;
  height: auto;
  min-height: 50px;
  font-size: 16px;
  font-weight: 700;
  text-transform: uppercase;
  color: ${Color('gray-5')};
  border-bottom: 1px solid ${Color('gray-3')};
  padding: 5px;
  box-sizing: border-box;
`
const Row = Header.extend`
  font-weight: 300;
  text-transform: none;

  &:hover { background-color: ${Color('gray-1')}; }
`
const CellId = styled.div`
  width: 200px;
`
const CellName = styled.div`
  width: 150px;
`
const CellEmail = styled.div`
  width: calc(100% - 600px);
`
const CellTime = styled.div`
  width: 150px;
`
const CellOptions = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  width: 100px;
`

const List = props => {
  const { users, handleCreate, handleDelete, handleEdit } = props

  return (
    <Wrapper>
      <Button onClick={() => handleCreate()}>Create User</Button>
      <Table>
        <Header>
          <CellId>Id</CellId>
          <CellName>Username</CellName>
          <CellEmail>Email</CellEmail>
          <CellTime>Created at</CellTime>
          <CellOptions>#</CellOptions>
        </Header>
        { users.map((user, index) =>
          <Row key={index}>
            <CellId>{user._id}</CellId>
            <CellName>{user.username}</CellName>
            <CellEmail>{user.email}</CellEmail>
            <CellTime>{moment(user.createdAt).format('DD/MM/YYYY HH:mm:ss')}</CellTime>
            <CellOptions>
              <Icon name='pencil' size='24px' color='gray-5' cursor onClick={() => handleEdit(user._id)} />
              <Icon name='trash' size='24px' color='gray-5' cursor onClick={() => handleDelete(user._id)} />
            </CellOptions>
          </Row>
        )}
      </Table>
    </Wrapper>
  )
}

List.propTypes = {
  users: PropTypes.array.isRequired
}

export default List
