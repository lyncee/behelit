import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { actions, selectors } from 'data'
import List from './template.js'

class ListContainer extends React.Component {
  constructor (props) {
    super(props)
    this.handleCreate = this.handleCreate.bind(this)
    this.handleDelete = this.handleDelete.bind(this)
    this.handleEdit = this.handleEdit.bind(this)
  }

  componentWillMount () {
    this.props.userActions.fetchUsers()
  }

  handleCreate () {
    this.props.routerActions.push('/admin/users/create')
  }

  handleDelete (id) {
    this.props.userActions.deleteUser(id)
  }

  handleEdit (id) {
    this.props.routerActions.push(`/admin/users/edit/${id}`)
  }

  render () {
    return <List {...this.props} handleCreate={this.handleCreate} handleDelete={this.handleDelete} handleEdit={this.handleEdit} />
  }
}

const mapStateToProps = (state) => ({
  users: selectors.admin.users.selectUsers(state)
})

const mapDispatchToProps = (dispatch) => ({
  routerActions: bindActionCreators(actions.router, dispatch),
  userActions: bindActionCreators(actions.admin.users, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(ListContainer)
