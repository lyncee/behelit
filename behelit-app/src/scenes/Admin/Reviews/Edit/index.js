import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { formValueSelector } from 'redux-form'

import { actions, selectors } from 'data'
import Edit from './template.js'

class EditContainer extends React.Component {
  constructor (props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentWillMount () {
    this.props.formActions.initialize('editReview', {
      name: this.props.review.name,
      company: this.props.review.company,
      role: this.props.review.role,
      message: this.props.review.message
    })
  }

  handleSubmit (e) {
    e.preventDefault()
    const { id, name, company, role, message } = this.props
    this.props.reviewActions.editReview(id, name, company, role, message)
  }

  render () {
    return <Edit title={this.props.name} onSubmit={this.handleSubmit} />
  }
}

const mapStateToProps = (state, ownProps) => ({
  name: formValueSelector('editReview')(state, 'name'),
  company: formValueSelector('editReview')(state, 'company'),
  role: formValueSelector('editReview')(state, 'role'),
  message: formValueSelector('editReview')(state, 'message'),
  review: selectors.admin.reviews.selectReview(ownProps.match.params.reviewId)(state),
  id: ownProps.match.params.reviewId
})

const mapDispatchToProps = (dispatch) => ({
  formActions: bindActionCreators(actions.form, dispatch),
  reviewActions: bindActionCreators(actions.admin.reviews, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(EditContainer)
