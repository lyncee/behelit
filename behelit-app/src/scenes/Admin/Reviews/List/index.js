import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { actions, selectors } from 'data'
import List from './template.js'

class ListContainer extends React.Component {
  constructor (props) {
    super(props)
    this.handleCreate = this.handleCreate.bind(this)
    this.handleDelete = this.handleDelete.bind(this)
    this.handleEdit = this.handleEdit.bind(this)
    this.handlePublish = this.handlePublish.bind(this)
    this.handleUnpublish = this.handleUnpublish.bind(this)
  }

  componentWillMount () {
    this.props.reviewActions.fetchReviews()
  }

  handleCreate () {
    this.props.routerActions.push('/admin/reviews/create')
  }

  handleDelete (id) {
    this.props.reviewActions.deleteReview(id)
  }

  handleEdit (id) {
    this.props.routerActions.push(`/admin/reviews/edit/${id}`)
  }

  handlePublish (id) {
    this.props.reviewActions.publishReview(id)
  }

  handleUnpublish (id) {
    this.props.reviewActions.unpublishReview(id)
  }

  render () {
    return <List
      {...this.props}
      handleCreate={this.handleCreate}
      handleDelete={this.handleDelete}
      handleEdit={this.handleEdit}
      handlePublish={this.handlePublish}
      handleUnpublish={this.handleUnpublish}
    />
  }
}

const mapStateToProps = (state) => ({
  reviews: selectors.admin.reviews.selectReviews(state)
})

const mapDispatchToProps = (dispatch) => ({
  reviewActions: bindActionCreators(actions.admin.reviews, dispatch),
  routerActions: bindActionCreators(actions.router, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(ListContainer)
