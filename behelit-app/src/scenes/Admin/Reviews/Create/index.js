import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { formValueSelector } from 'redux-form'

import { actions } from 'data'
import Create from './template.js'

class CreateContainer extends React.Component {
  constructor (props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit (e) {
    e.preventDefault()
    const { name, company, role, message } = this.props
    this.props.reviewActions.createReview(name, company, role, message)
  }

  render () {
    return <Create handleSubmit={this.handleSubmit} />
  }
}

const mapStateToProps = (state, ownProps) => ({
  name: formValueSelector('createReview')(state, 'name'),
  company: formValueSelector('createReview')(state, 'company'),
  role: formValueSelector('createReview')(state, 'role'),
  message: formValueSelector('createReview')(state, 'message')
})

const mapDispatchToProps = (dispatch) => ({
  formActions: bindActionCreators(actions.form, dispatch),
  reviewActions: bindActionCreators(actions.admin.reviews, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(CreateContainer)
