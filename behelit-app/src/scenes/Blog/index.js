import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { actions, selectors } from 'data'
import Blog from './template.js'

class BlogContainer extends React.Component {
  constructor (props) {
    super(props)
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick (blogPostId) {
    this.props.routerActions.push(`/blog/${blogPostId}`)
  }

  componentWillMount () {
    this.props.articleActions.fetchArticles()
  }

  render () {
    return <Blog {...this.props} handleClick={this.handleClick} />
  }
}

const mapStateToProps = (state) => ({
  articles: selectors.articles.selectArticles(state)
})

const mapDispatchToProps = (dispatch) => ({
  articleActions: bindActionCreators(actions.articles, dispatch),
  routerActions: bindActionCreators(actions.router, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(BlogContainer)
