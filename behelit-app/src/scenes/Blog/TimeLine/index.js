import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { groupBy, prop } from 'ramda'

import { actions, selectors } from 'data'
import Timeline from './template.js'

class TimelineContainer extends React.Component {
  transform (articles) {
    return groupBy(x => prop('category', x), articles)
  }

  render () {
    return <Timeline articles={this.transform(this.props.articles)} handleClick={this.props.handleClick} />
  }
}

const mapStateToProps = (state) => ({
  articles: selectors.articles.selectArticles(state)
})

const mapDispatchToProps = (dispatch) => ({
  articleActions: bindActionCreators(actions.articles, dispatch),
  routerActions: bindActionCreators(actions.router, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(TimelineContainer)
