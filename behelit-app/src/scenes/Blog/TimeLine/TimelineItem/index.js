import React from 'react'
import PropTypes from 'prop-types'
import ui from 'redux-ui'

import TimelineItem from './template.js'

class TimelineItemContainer extends React.Component {
  constructor (props) {
    super(props)
    this.handleToggle = this.handleToggle.bind(this)
  }

  handleToggle () {
    this.props.updateUI({ toggled: !this.props.ui.toggled })
  }

  render () {
    const { category, articles, ui, handleClick } = this.props

    return <TimelineItem
      category={category}
      articles={articles}
      toggled={ui.toggled}
      handleClick={handleClick}
      handleToggle={this.handleToggle}
     />
  }
}

TimelineItemContainer.propTypes = {
  category: PropTypes.string.isRequired,
  articles: PropTypes.array.isRequired,
  handleClick: PropTypes.func.isRequired
}

export default ui({ state: { toggled: false } })(TimelineItemContainer)
