import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { Color, Link } from 'behelit-components'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;
  background-color: ${Color('gray-1')};
  margin-bottom: 10px;
`
const Articles = styled.ul`
  display: ${props => props.displayed ? 'flex' : 'none'};
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;
  padding: 0;
  margin: 10px;
  list-style: none;
`
const ArticleLink = styled.li`
  width: 100%;
  margin-left: 10px;
  margin-bottom: 10px;
  cursor: pointer;
  &:hover { color: ${Color('brand-primary')}; }
`

const TimelineItem = props => {
  const { category, articles, toggled, handleToggle, handleClick } = props

  return (
    <Wrapper>
      <Link size='18px' weight={500} color='brand-primary' uppercase onClick={handleToggle}>{category}</Link>
      <Articles displayed={toggled}>
        {articles.map((article, index) => <ArticleLink key={index} size='16px' weight={300} onClick={() => handleClick(article._id)}>{article.title}</ArticleLink>)}
      </Articles>
    </Wrapper>
  )
}

TimelineItem.propTypes = {
  category: PropTypes.string,
  articles: PropTypes.array
}

export default TimelineItem
