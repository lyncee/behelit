import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { keys, prop } from 'ramda'

import { Color, Text } from 'behelit-components'
import TimelineItem from './TimelineItem'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;
  padding: 15px;
  box-sizing: border-box;
  background-color: ${Color('gray-1')};
  border: 1px solid ${Color('gray-2')};
  border-radius: 5px;
`
const Header = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: align-items;
  width: 100%;
  margin-bottom: 15px;
`
const Content = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;
`

const Timeline = props => {
  const { articles, handleClick } = props

  return (
    <Wrapper>
      <Header>
        <Text size='20px' weight={500} color='gray-6' uppercase>Articles</Text>
      </Header>
      <Content>
        { keys(articles).map((category, index) => <TimelineItem key={index} category={category} articles={prop(category, articles)} handleClick={handleClick} />)}
      </Content>
    </Wrapper>
  )
}

Timeline.propTypes = {
  articles: PropTypes.object,
  handleClick: PropTypes.func
}

export default Timeline
