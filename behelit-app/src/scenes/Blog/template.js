import React from 'react'
import styled from 'styled-components'
import { isEmpty } from 'ramda'

import { SplashScreen } from 'components'
import BlogList from './BlogList'
import Timeline from './Timeline'

const Wrapper = styled.section`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;
`
const ColumnLeft = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;
  
  @media(min-width: 992px) {
    width: 70%;
  }
`
const ColumnRight = styled.div`
  display: none;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  margin-left: 10px;

  @media(min-width: 992px) {
    display: flex;
    width: 30%;
  }
`

const Blog = props => !isEmpty(props.articles) ? (
  <Wrapper>
    <ColumnLeft>
      <BlogList {...props} />
    </ColumnLeft>
    <ColumnRight>
      <Timeline {...props} />
    </ColumnRight>
  </Wrapper>
) : (
  <Wrapper>
    <SplashScreen />
  </Wrapper>
)

export default Blog
