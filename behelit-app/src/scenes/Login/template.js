import React from 'react'
import styled from 'styled-components'
import { reduxForm, Field } from 'redux-form'

import { isRequired } from 'services/form'
import { Button, Panel, Text } from 'behelit-components'
import { Form, TextBox, PasswordBox } from 'components'

const Wrapper = styled.section`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
`
const Container = styled.div`
  width: 100%;

  @media(min-width: 992px) { width: 500px; }
`

const Contact = props => {
  const { onSubmit, submitting, invalid } = props

  return (
    <Wrapper>
      <Container>
        <Panel>
          <Form onSubmit={onSubmit}>
            <Text size='16px' weight={500}>Username:</Text>
            <Field name='username' component={TextBox} validate={[isRequired]} />
            <Text size='16px' weight={500}>Password:</Text>
            <Field name='password' component={PasswordBox} validate={[isRequired]} />
            <Button fullwidth disabled={submitting || invalid}>Login</Button>
          </Form>
        </Panel>
      </Container>
    </Wrapper>
  )
}

export default reduxForm({ form: 'login' })(Contact)
