import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { formValueSelector } from 'redux-form'

import { actions } from 'data'
import Login from './template'

class LoginContainer extends React.Component {
  constructor (props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit (e) {
    e.preventDefault()
    const { username, password } = this.props
    this.props.authActions.login(username, password)
  }

  render () {
    return <Login onSubmit={this.handleSubmit} {...this.props} />
  }
}

const mapStateToProps = (state, ownProps) => ({
  username: formValueSelector('login')(state, 'username'),
  password: formValueSelector('login')(state, 'password')
})

const mapDispatchToProps = (dispatch) => ({
  authActions: bindActionCreators(actions.auth, dispatch),
  formActions: bindActionCreators(actions.form, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer)
