import React from 'react'
import { reduxForm, Field } from 'redux-form'

import { isRequired } from 'services/form'
import { Button, Panel, Text, TextGroup } from 'behelit-components'
import { Form, TextBox, TextAreaBox } from 'components'

const Reviews = props => {
  const { onSubmit, pristine, dirty, invalid } = props

  return (
    <Panel>
      <Text size='20px' weight={700} color='brand-primary' uppercase>Reviews</Text>
      <TextGroup>
        <Text>If we had the chance to work together, don't hesitate to write a review !</Text>
      </TextGroup>
      <Form onSubmit={onSubmit}>
        <Text size='16px' weight={500}>Name:</Text>
        <Field name='name' component={TextBox} validate={[isRequired]} />
        <Text size='16px' weight={500}>Company:</Text>
        <Field name='company' component={TextBox} validate={[isRequired]} />
        <Text size='16px' weight={500}>Role:</Text>
        <Field name='role' component={TextBox} />
        <Text size='16px' weight={500}>Comment:</Text>
        <Field name='message' component={TextAreaBox} validate={[isRequired]} rows={5} />
        <Button fullwidth disabled={pristine || (dirty && invalid)}>Send Review</Button>
      </Form>
    </Panel>
  )
}

export default reduxForm({ form: 'review' })(Reviews)
