import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { formValueSelector } from 'redux-form'

import { actions } from 'data'
import ReviewForm from './template'

class ReviewFormContainer extends React.Component {
  constructor (props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit (e) {
    e.preventDefault()
    const { name, company, role, message } = this.props
    this.props.reviewActions.createReview(name, company, role, message)
  }

  render () {
    return <ReviewForm onSubmit={this.handleSubmit} {...this.props} />
  }
}

const mapStateToProps = (state, ownProps) => ({
  name: formValueSelector('review')(state, 'name'),
  company: formValueSelector('review')(state, 'company'),
  role: formValueSelector('review')(state, 'role'),
  message: formValueSelector('review')(state, 'message')
})

const mapDispatchToProps = (dispatch) => ({
  reviewActions: bindActionCreators(actions.reviews, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(ReviewFormContainer)
