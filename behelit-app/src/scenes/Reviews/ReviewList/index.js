import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { actions, selectors } from 'data'
import ReviewList from './template'

class ReviewListContainer extends React.Component {
  componentWillMount () {
    this.props.reviewActions.fetchReviews()
  }

  render () {
    return <ReviewList reviews={this.props.reviews} />
  }
}

const mapStateToProps = (state) => ({
  reviews: selectors.reviews.selectReviews(state)
})

const mapDispatchToProps = (dispatch) => ({
  reviewActions: bindActionCreators(actions.reviews, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(ReviewListContainer)
