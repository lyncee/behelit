import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { Quote, Text } from 'behelit-components'

const Wrapper = styled.div`
  margin-bottom: 10px;
  width: 100%;
`
const Row = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  text-align: justify;
  width: 100%;
  margin-bottom: 10px;
`

const ReviewItem = props => {
  const { review } = props
  const description = review.role ? `${review.role} @ ${review.company}` : review.company

  return (
    <Wrapper>
      <Quote>
        <Row>
          <Text size='16px' italic>{review.message}</Text>
        </Row>
        <Row>
          <Text size='16px' weight={500}>{review.name}</Text>
          <Text size='16px' italic>{review.createdAt}</Text>
        </Row>
        <Row>
          <Text size='16px'>{description}</Text>
        </Row>
      </Quote>
    </Wrapper>
  )
}

ReviewItem.propTypes = {
  review: PropTypes.shape({
    name: PropTypes.string.isRequired,
    company: PropTypes.string.isRequired,
    message: PropTypes.string.isRequired,
    createdAt: PropTypes.string.isRequired,
    role: PropTypes.string
  }).isRequired
}

export default ReviewItem
