import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import ReviewItem from './ReviewItem'

const Wrapper = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;
`
const ReviewList = props => {
  const { reviews } = props

  return (
    <Wrapper>
      { reviews.map((review, index) => <ReviewItem review={review} key={index} />) }
    </Wrapper>
  )
}

ReviewList.propTypes = {
  reviews: PropTypes.array.isRequired
}

export default ReviewList
