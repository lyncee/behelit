import React from 'react'
import styled from 'styled-components'

import ReviewForm from './ReviewForm'
import ReviewList from './ReviewList'

const Wrapper = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;

  @media(min-width: 992px) { flex-direction: row; }

  & > :first-child { margin-right: 10px; }
`
const Container = styled.div`
  width: 100%;
  margin-bottom: 10px;

  @media(min-width: 992px) { width: 50%; }
`

const Reviews = props => (
  <Wrapper>
    <Container>
      <ReviewForm />
    </Container>
    <Container>
      <ReviewList />
    </Container>
  </Wrapper>
)

export default Reviews
