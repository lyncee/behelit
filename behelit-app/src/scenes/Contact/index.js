import React from 'react'
import { formValueSelector } from 'redux-form'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { actions } from 'data'

import Contact from './template.js'

class ContactContainer extends React.Component {
  constructor (props) {
    super(props)
    this.subjects = [
      { text: 'General enquiries', value: 'General enquiries' },
      { text: 'Commission request', value: 'Commission request' },
      { text: 'Other', value: 'Other' }
    ]
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit (e) {
    e.preventDefault()
    const { name, address, subject, message } = this.props
    this.props.actions.sendEmail({ name, address, message, subject })
  }

  render () {
    return <Contact subjects={this.subjects} onSubmit={this.handleSubmit} {...this.props} />
  }
}

const mapStateToProps = (state) => ({
  name: formValueSelector('contact')(state, 'name'),
  address: formValueSelector('contact')(state, 'address'),
  message: formValueSelector('contact')(state, 'message'),
  subject: formValueSelector('contact')(state, 'subject')
})

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(actions.application, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(ContactContainer)
