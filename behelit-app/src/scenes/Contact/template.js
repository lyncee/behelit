import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { reduxForm, Field } from 'redux-form'

import { isRequired, isRequiredEmail } from 'services/form'
import { Button, Panel, Text, TextGroup } from 'behelit-components'
import { Form, SelectBox, TextBox, TextAreaBox } from 'components'

const Wrapper = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;
`
const Container = styled.div`
  width: 100%;

  @media(min-width: 992px) { width: 50%; }
`

const Contact = props => {
  const { subjects, onSubmit, submitting, invalid } = props

  return (
    <Wrapper>
      <Container>
        <Panel>
          <Text size='20px' weight={700} color='brand-primary' uppercase>Contact</Text>
          <TextGroup>
            <Text>The best way to get in touch with me is to use this form.</Text>
            <Text>Please be aware that I only accept commissioned work on occasion depending on my schedule.</Text>
          </TextGroup>
          <Form onSubmit={onSubmit}>
            <Text size='16px' weight={500}>Subject:</Text>
            <Field name='subject' component={SelectBox} validate={[isRequired]} items={subjects} />
            <Text size='16px' weight={500}>Name:</Text>
            <Field name='name' component={TextBox} validate={[isRequired]} />
            <Text size='16px' weight={500}>Address:</Text>
            <Field name='address' component={TextBox} validate={[isRequiredEmail]} />
            <Text size='16px' weight={500}>Message:</Text>
            <Field name='message' component={TextAreaBox} validate={[isRequired]} rows={5} />
            <Button fullwidth disabled={submitting || invalid}>Send Message</Button>
          </Form>
        </Panel>
      </Container>
    </Wrapper>
  )
}

Contact.propTypes = {
  subjects: PropTypes.array.isRequired
}

export default reduxForm({ form: 'contact' })(Contact)
