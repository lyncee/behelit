import React from 'react'
import styled from 'styled-components'

import { Color, Link, List, ListItem, Panel, Text, TextGroup } from 'behelit-components'

const Wrapper = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;
`
const Row = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;
  margin-bottom: 15px;

  @media(min-width: 992px) { flex-direction: row; }
`
const Column = styled.div`
  width: 100%;
  padding: 15px;
  box-sizing: border-box;
  @media(min-width: 992px) { width: ${props => props.left ? '55%' : '45%'}; }
`
const Title = styled.div`
  width: auto;
  margin-bottom: 15px;
  border-bottom: 1px solid ${Color('brand-primary')};
`
const Content = styled.div`
  margin-bottom: 15px;
  & > ul { padding: 10px; }
`

const About = () => {
  return (
    <Wrapper>
      <Panel>
        <Row>
          <Column left>
            <Title>
              <Text size='20px' weight={700} color='brand-primary' uppercase>Professional History</Text>
            </Title>
            <Content>
              <TextGroup>
                <Text>
                  I started working in France in 2008 for <strong>Scotler</strong>, a Computer Services Company, as a consultant specialising in SharePoint solutions.
                  This experience allowed me to gain great experience in SharePoint as well as .NET & front-end development.
                  You could say it really began my love affair with tech.
                  Most importantly, I worked with customers who weren't tech literate and so forced me to learn how to translate their requirements into software solutions.
              </Text>
                <Text>
                  In 2011, I moved to Hong Kong for a long-term mission.
                  <strong>TV5 Monde</strong> a french TV channel, hired me in order to review and renew their entire South Asian web presence.
                  The role was very challenging particularly as I was the only software engineer.
                  However, I soon successfully produced 4 new multilingual websites based on Kentico .NET.
                </Text>
                <Text>
                  Around the same time, I also started a personal project named <strong>REGIF.NET</strong>, a social network to create and share .GIF pictures.
                  The most interesting part was a feature to customize .GIFs.
                  Using vanilla javascript, this feature was able to decrypt any GIF file in order to split each frame in a gallery.
                  It was then possible to apply pixels transformations on each frame using HTML5 Canvas, and finally re-encrypting everything back into a .GIF format.
                </Text>
                <Text>
                  In 2015, I decided to come back to Europe.
                  I moved to the UK and started a new job at <strong>Capital on Tap</strong>, a small but successful FinTech company.
                  I had a great time working in and addressing the unique challenges involved in a startup environment.
                </Text>
                <Text>
                  In 2017, I joined the company <strong>Blockchain</strong>, that is providing crypto currencies wallet.
                  Currently focused on front-end, I am now working on ReactJS full-time to grow my knowledge and deliver the most scalable and maintainable front-end wallet possible.
                </Text>
                <Text>
                  I decided to continue my self-learning and also help businesses by providing guidance and best practices on various technologies.
                </Text>
              </TextGroup>
            </Content>
          </Column>
          <Column>
            <Title>
              <Text size='20px' weight={700} color='brand-primary' uppercase>About this website</Text>
            </Title>
            <Content>
              <TextGroup inline>
                <Text>
                  Early 2017, I created this website in order to promote my projects.
                  It was also the perfect opportunity to develop a web application in React using the following key points :
                </Text>
              </TextGroup>
              <List>
                <ListItem>React ES6</ListItem>
                <ListItem>Redux Saga</ListItem>
                <ListItem>.NET Core API</ListItem>
                <ListItem>Sass</ListItem>
                <ListItem>Bootstrap</ListItem>
                <ListItem>Webpack</ListItem>
                <ListItem>Webstorm</ListItem>
                <ListItem>AWS</ListItem>
              </List>
              <TextGroup inline>
                <Text>
                  If you wish to know more about it, I also released the source code at this GIT repository address:
                </Text>
                <Link href='https://gitlab.com/lyncee/behelit' target='_blank'>https://gitlab.com/lyncee/behelit</Link>
              </TextGroup>
            </Content>
            <Title>
              <Text size='20px' weight={700} color='brand-primary' uppercase>Workstation</Text>
            </Title>
            <Content>
              <List>
                <ListItem>Intel Core i7 6700K @4GHz</ListItem>
                <ListItem>Asus Z170 Pro Gaming</ListItem>
                <ListItem>Corsair Vengeance PC24000 3GHz 16GB DDR4 x2</ListItem>
                <ListItem>Corsair Hydro Series H60 2013</ListItem>
                <ListItem>MSI GeForce GTX 1060 3GB DDR5</ListItem>
                <ListItem>SanDisk Ultra II 480GB SATA III</ListItem>
                <ListItem>Evga Supernova 650 G3</ListItem>
                <ListItem>DELL P2417H 23.8' x4</ListItem>
                <ListItem>Roccat ISKU FX</ListItem>
                <ListItem>Roccat KONE XTD</ListItem>
              </List>
            </Content>
            <Title>
              <Text size='20px' weight={700} color='brand-primary' uppercase>Beloved Software</Text>
            </Title>
            <Content>
              <List>
                <ListItem>Microsoft Visual Studio Code</ListItem>
                <ListItem>Microsoft Visual Studio 2015 Update 3</ListItem>
                <ListItem>JetBrains ReSharper</ListItem>
                <ListItem>JetBrains WebStorm</ListItem>
                <ListItem>JetBrains Rider</ListItem>
                <ListItem>Paint .NET</ListItem>
              </List>
            </Content>
          </Column>
        </Row>
      </Panel>
    </Wrapper>
  )
}

export default About
