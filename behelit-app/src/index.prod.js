import React from 'react'
import ReactDOM from 'react-dom'
import App from 'scenes/app.js'
import configureStore from 'store'
import './favicon.ico'

const { store, history } = configureStore()

ReactDOM.render(
  <App store={store} history={history} />,
  document.getElementById('app')
)
