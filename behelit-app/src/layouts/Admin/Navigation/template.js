import React from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'

import { Navbar, NavbarBrand, NavbarHeader, NavbarMenu, NavbarNav, NavbarNavItem, NavbarToggler, Text } from 'behelit-components'
import NavbarLoader from 'components/NavbarLoader'

const Navigation = (props) => {
  const { toggled, handleToggle } = props

  return (
    <Navbar height='60px' fluid>
      <NavbarHeader>
        <NavbarBrand>
          <NavLink to='/'>
            <Text size='48px' color='brand-primary'>Behelit</Text>
          </NavLink>
        </NavbarBrand>
      </NavbarHeader>
      <NavbarMenu toggled={toggled}>
        <NavbarNav>
          <NavbarNavItem>
            <NavbarLoader />
          </NavbarNavItem>
        </NavbarNav>
      </NavbarMenu>
      <NavbarToggler onToggle={handleToggle} toggled={toggled} />
    </Navbar>
  )
}

Navigation.propTypes = {
  handleToggle: PropTypes.func.isRequired,
  toggled: PropTypes.bool.isRequired
}

export default Navigation
