import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { NavLink } from 'react-router-dom'

import { Color } from 'behelit-components'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;
  height: 100%;
`
const MenuItem = styled(NavLink)`
  font-family: 'Roboto', sans serif;
  font-size: 18px;
  font-weight: 400;
  color: ${Color('gray-5')};
  text-decoration: none;
  width: 100%;
  padding: 15px;
  box-sizing: border-box;

  &:hover {
    background-color: ${Color('gray-3')};
  }

  &.active {
    font-weight: 700;
    color: ${Color('brand-primary')};
  }
`

const Menu = (props) => {
  return (
    <Wrapper>
      <MenuItem to='/admin/articles' activeClassName='active'>Articles</MenuItem>
      <MenuItem to='/admin/comments' activeClassName='active'>Comments</MenuItem>
      <MenuItem to='/admin/projects' activeClassName='active'>Projects</MenuItem>
      <MenuItem to='/admin/reviews' activeClassName='active'>Reviews</MenuItem>
      <MenuItem to='/admin/tags' activeClassName='active'>Tags</MenuItem>
      <MenuItem to='/admin/users' activeClassName='active'>Users</MenuItem>
    </Wrapper>
  )
}

Menu.propTypes = {
  handleToggle: PropTypes.func.isRequired,
  toggled: PropTypes.bool.isRequired
}

export default Menu
