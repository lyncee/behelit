import React from 'react'
import ui from 'redux-ui'

import Menu from './template.js'

class MenuContainer extends React.Component {
  constructor (props) {
    super(props)
    this.handleToggle = this.handleToggle.bind(this)
    this.handleClose = this.handleClose.bind(this)
  }

  handleToggle () {
    this.props.updateUI({ toggled: this.props.ui.toggled })
  }

  handleClose () {
    this.props.updateUI({ toggled: false })
  }

  render () {
    return <Menu toggled={this.props.ui.toggled} handleToggle={this.handleToggle} handleClose={this.handleClose} />
  }
}

export default ui({ key: 'MenuAdmin', state: { toggled: false } })(MenuContainer)
