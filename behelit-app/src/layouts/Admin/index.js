import React from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { Redirect, Route } from 'react-router-dom'

import background from './fresh_snow.png'
import { selectors } from 'data'
import { Color, Container } from 'behelit-components'
import Alerts from './Alerts'
import Menu from './Menu'
import Navigation from './Navigation'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  height: 100%;
  width: 100%;
  background-color: ${Color('gray-2')};
  background-image: url('${background}');
  background-repeat: repeat-both;
  bottom: 0;
`
const Header = styled.div`
  width: 100%;
  height: 60px;
  background-color: ${Color('gray-6')};
`
const Content = styled(Container)`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;
  height: calc(100% - 60px);
`
const MenuContainer = styled.div`
  flex: 80px;
  height: 100%;
  border-right: 1px solid ${Color('gray-3')};
`
const Page = styled.div`
  flex: calc(100% - 80px);
  height: 100%;
  padding: 15px;
  box-sizing: border-box;
`

const AdminLayout = (props) => {
  const { component: Component, username, token, ...rest } = props
  const isAuthenticated = username && token
  // const isAuthenticated = true

  return isAuthenticated ? (
    <Route {...rest} render={props => (
      <Wrapper>
        <Alerts />
        <Header>
          <Navigation />
        </Header>
        <Content>
          <MenuContainer>
            <Menu location={props.location} token={token} />
          </MenuContainer>
          <Page>
            <Component {...props} />
          </Page>
        </Content>
      </Wrapper>
    )} />) : (
      <Redirect to='/login' />
    )
}

const mapStateToProps = state => ({
  username: selectors.auth.selectUsername(state),
  token: selectors.auth.selectToken(state)
})

export default connect(mapStateToProps)(AdminLayout)
