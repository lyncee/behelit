import React from 'react'
import styled from 'styled-components'
import { Route } from 'react-router-dom'

import { Color, Container } from 'behelit-components'
import Alerts from './Alerts'
import Navigation from './Navigation'
import background from './fresh_snow.png'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flexs-start;
  align-items: flex-start;
  min-height: 100%;
  width: 100%;
  background-color: ${Color('gray-2')};
  background-image: url('${background}');
  background-repeat: repeat-both;
  bottom: 0;
`
const Header = styled.div`
  width: 100%;
  height: 60px;
  background-color: ${Color('gray-6')};
  z-index: 1;
`
const Content = styled(Container)`
  height: calc(100% - 60px);
  padding: 15px;
  box-sizing: border-box;
`

const AdminLayout = ({component: Component, location, ...rest}) => (
  <Route {...rest} render={props => (
    <Wrapper>
      <Header>
        <Navigation location={props.location} />
      </Header>
      <Alerts />
      <Content>
        <Container>
          <Component {...props} />
        </Container>
      </Content>
    </Wrapper>
  )} />
)

export default AdminLayout
