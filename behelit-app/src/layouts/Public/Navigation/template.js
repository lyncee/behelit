import React from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'

import { Icon, Link, Navbar, NavbarBrand, NavbarHeader, NavbarMenu, NavbarNav, NavbarNavItem, NavbarToggler, Text } from 'behelit-components'
import NavbarLoader from 'components/NavbarLoader'

const Navigation = (props) => {
  const { toggled, handleToggle, handleClose } = props

  return (
    <Navbar height='60px' fluid>
      <NavbarHeader>
        <NavbarBrand>
          <NavLink to='/blog'>
            <Text size='48px' color='brand-primary' altFont>Behelit</Text>
          </NavLink>
        </NavbarBrand>
      </NavbarHeader>
      <NavbarMenu toggled={toggled}>
        <NavbarNav>
          <NavbarNavItem>
            <NavLink to='/blog' onClick={handleClose}>Blog</NavLink>
          </NavbarNavItem>
          <NavbarNavItem>
            <NavLink to='/projects' onClick={handleClose}>Projects</NavLink>
          </NavbarNavItem>
          <NavbarNavItem>
            <NavLink to='/reviews' onClick={handleClose}>Reviews</NavLink>
          </NavbarNavItem>
          <NavbarNavItem>
            <NavLink to='/contact' onClick={handleClose}>Contact</NavLink>
          </NavbarNavItem>
          <NavbarNavItem>
            <NavLink to='/about' onClick={handleClose}>About</NavLink>
          </NavbarNavItem>
        </NavbarNav>
        <NavbarNav>
          <NavbarNavItem>
            <NavbarLoader />
          </NavbarNavItem>
          <NavbarNavItem>
            <Link href='https://www.linkedin.com/in/guillaume-marquilly-0403945b' target='_blank'>
              <Icon name='linkedin-square' size='32px' color='dark-cyan' cursor hover />
            </Link>
          </NavbarNavItem>
        </NavbarNav>
      </NavbarMenu>
      <NavbarToggler onToggle={handleToggle} toggled={toggled} />
    </Navbar>
  )
}

Navigation.propTypes = {
  handleToggle: PropTypes.func.isRequired,
  toggled: PropTypes.bool.isRequired
}

export default Navigation
