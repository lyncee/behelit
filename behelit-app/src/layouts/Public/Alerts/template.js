import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Toast } from 'behelit-components'

const Wrapper = styled.div`
  position: absolute;
  bottom: 0;
  left: 0;
  width: 100%;
  display: flex;
  flex-direction: column-reverse;
  justify-content: flex-start;
  align-items: flex-start;
  z-index: 5;

  & > * { margin-top: 5px; }

  @media(min-width: 768px) { 
    bottom: 5px;
    left: 5px;
    width: auto;
  }
`

const Alerts = props => {
  const { alerts, handleClose } = props

  return (
    <Wrapper>
      { alerts.map((alert, index) => {
        const { id, nature, message } = alert
        return <Toast key={index} nature={nature} onClose={() => handleClose(id)}>{message}</Toast>
      })}
    </Wrapper>
  )
}

Alerts.propTypes = {
  alerts: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    nature: PropTypes.oneOf(['info', 'error', 'success']),
    message: PropTypes.string.isRequired,
    data: PropTypes.object
  }))
}

export default Alerts
