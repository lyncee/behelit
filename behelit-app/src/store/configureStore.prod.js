import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { createBrowserHistory } from 'history'
import { connectRouter, routerMiddleware } from 'connected-react-router'
import { rootReducer, rootSaga } from 'data'

export default function configureStore (initialState) {
  const history = createBrowserHistory()
  const sagaMiddleware = createSagaMiddleware()
  const mainReducer = connectRouter(history)(rootReducer)
  const reduxRouterMiddleware = routerMiddleware(history)

  const store = createStore(
    mainReducer,
    initialState,
    applyMiddleware(
      reduxRouterMiddleware,
      sagaMiddleware
    )
  )

  sagaMiddleware.run(rootSaga)

  return { store, history }
}
