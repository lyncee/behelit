import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { createBrowserHistory } from 'history'
import { connectRouter, routerMiddleware } from 'connected-react-router'
import { rootReducer, rootSaga } from 'data'

const devToolsConfig = {
  maxAge: 1000
}

export default function configureStore (initialState) {
  const history = createBrowserHistory()
  const sagaMiddleware = createSagaMiddleware()
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__(devToolsConfig) : compose
  const mainReducer = connectRouter(history)(rootReducer)
  const reduxRouterMiddleware = routerMiddleware(history)

  const store = createStore(
    mainReducer,
    initialState,
    composeEnhancers(
      applyMiddleware(
        reduxRouterMiddleware,
        sagaMiddleware
      )
    )
  )

  sagaMiddleware.run(rootSaga)

  return { store, history }
}
