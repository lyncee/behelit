## Introduction

This codebase contains a React application available at http://www.behelit.uk

It features:

* React ES6
* Redux
* Redux Saga
* styled-components
* redux-ui
* redux-form
* monorepo architecture
* storybook
* content management interface
* blog engine (using a markdown engine)
* token based authentication

## Commands

* `yarn install`: restore dependencies
* `yarn start`: start the application using webpack-dev-server (development mode)
* `yarn build`: create the release in the [dist] folder.
* `yarn storybook`: start the storybook to visualize the generic components available with code samples.